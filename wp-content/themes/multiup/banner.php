<!-- Header banner -->
<?php 
	global $post;
	$post_id = $post->ID;
	$banner_title = rs::getField('rst_banner_title',$post_id) ? rs::getField('rst_banner_title',$post_id) : get_the_title();
	$banner_subtitle = rs::getField('rst_banner_subtitle',$post_id);
	$header_layout = rs::getField('rst_header_layout',$post_id) ? rs::getField('rst_header_layout',$post_id) : 1;
	$container = rs::getField('rst_select_banner_container',$post_id) ? rs::getField('rst_select_banner_container',$post_id) : 'smallcontainer'; 
	$video_link = rs::getField('rst_banner_vimeolink',$post_id); 
	$revslides = rs::getField('rst_banner_revslides',$post_id); 
	$work = rs::getField('rst_header_banner_button',$post_id);
	if ( $header_layout == '1') {
?>

<div class="rst-header-banner">
	<div class="rst-banner-content">
		<div class="rst-banner-title">
			<div class="container">
				<h1><?php echo force_balance_tags($banner_title) ?></h1>
				<p><?php echo force_balance_tags($banner_subtitle) ?></p>
				<?php
					if( is_array($work) ) {
						foreach ( $work as $item ) {
				?>
					<a class="btn btn-success btn-transparent" href="<?php echo esc_url($item['rst_button_link']); ?>"><?php echo esc_html($item['rst_button_text']) ?></a>
				<?php 
						}
					}
				?>
			</div>
		</div>
		<div class="rst-scroll-down">
			<a href="#"><i class="fa fa-chevron-down"></i></a>
		</div>
	</div>
</div>

<?php } else if ( $header_layout == '2') { ?>
	
	<div class="rst-banner-content rst-index-banner-content <?php
		if ( $container == 'smallcontainer' ) {
			echo 'rst-banner-small-container';
		}
	?>">
		<?php
			if ( substr($video_link, -12, 1) == '=' )
			{
				$link = 'https://www.youtube.com/embed/';
				$link .= substr($video_link, -11);
			} else {
				$link_id = explode("/", $video_link);
				$link_id = $link_id[sizeof($link_id)-1];
				$link = 'https://player.vimeo.com/video/';
				$link .= $link_id;
			}
		?>
		<?php 
			$showvideos = rs::getField('rst_showvideo');
			if ( $showvideos == 'show' )
			{
		?>
			<a class="rst-banner-video fancybox-media control-video" href="<?php echo esc_url($link);?>"><i class="fa fa-play"></i></a>
		<?php } ?>
		<div class="rst-banner-title">
			<div class="container">
				<h1><?php echo force_balance_tags($banner_title); ?></h1>
				<p><?php echo force_balance_tags($banner_subtitle) ?></p>
				<?php
					if( is_array($work) ) {
						foreach ( $work as $item ) {
				?>
					<a class="btn btn-success btn-transparent" href="<?php echo esc_url($item['rst_button_link']); ?>"><?php echo esc_html($item['rst_button_text']) ?></a>
				<?php 
						}
					}
				?>
			</div>
		</div>
		<div class="rst-scroll-down">
			<a href="#"><i class="fa fa-chevron-down"></i></a>
		</div>
	</div>
	
<?php } elseif( $header_layout == 3 ) { ?>
	<div class="revslides">
		<?php echo $revslides ? do_shortcode($revslides) : '' ?>
	</div>
<?php } ?>