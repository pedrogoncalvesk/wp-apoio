<?php if( is_active_sidebar( 'sidebar-1' ) || is_active_sidebar( 'sidebar-2' ) || is_active_sidebar( 'sidebar-3' ) || is_active_sidebar( 'sidebar-4' ) ) { ?>
<div class="row">
	<div class="col-xs-3">
		<?php
			if ( is_active_sidebar( 'sidebar-1' ) ) {
				dynamic_sidebar( 'sidebar-1' );
			}
		?>
	</div>
	<div class="col-xs-3">
		<?php
			if ( is_active_sidebar( 'sidebar-2' ) ) {
				dynamic_sidebar( 'sidebar-2' );
			}
		?>
	</div>
	<div class="col-xs-3">
		<?php
			if ( is_active_sidebar( 'sidebar-3' ) ) {
				dynamic_sidebar( 'sidebar-3' );
			}
		?>
	</div>
	<div class="col-xs-3">
		<div class="rst-happy-hours">
			<?php
				if ( is_active_sidebar( 'sidebar-4' ) ) {
					dynamic_sidebar( 'sidebar-4' );
				}
			?>
		</div>
	</div>
</div>
<!-- End row -->
<?php } ?>