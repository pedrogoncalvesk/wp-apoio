<?php if( is_active_sidebar( 'sidebar-1' ) ) { ?>
<div class="row">
	<div class="col-xs-12">
		<?php
			if ( is_active_sidebar( 'sidebar-1' ) ) {
				dynamic_sidebar( 'sidebar-1' );
			}
		?>
	</div>
</div>
<!-- End row -->
<?php } ?>