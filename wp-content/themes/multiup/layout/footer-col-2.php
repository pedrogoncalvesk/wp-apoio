<?php if( is_active_sidebar( 'sidebar-1' ) || is_active_sidebar( 'sidebar-2' ) ) { ?>
<div class="row">
	<div class="col-xs-6">
		<?php
			if ( is_active_sidebar( 'sidebar-1' ) ) {
				dynamic_sidebar( 'sidebar-1' );
			}
		?>
	</div>
	<div class="col-xs-6">
		<?php
			if ( is_active_sidebar( 'sidebar-2' ) ) {
				dynamic_sidebar( 'sidebar-2' );
			}
		?>
	</div>
</div>
<!-- End row -->
<?php } ?>