<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Ublog
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
} 
?>
<?php if ( have_comments() ) : ?>
<div id="comments" class="comments-area rst-comment-box rst-wrap-content wow fadeIn animated">
	<div class="rst-remove"><?php wp_list_comments();paginate_comments_links();next_comments_link(); ?></div>
	<?php // You can start editing here -- including this comment! ?>
	<h2><?php comments_number('COMMENTS', 'COMMENTS', 'COMMENTS');?></h2>
	<?php rst_comments(get_the_ID()); ?>
	
</div><!-- #comments -->
<?php endif; ?>

<div class="rst-comment-form rst-wrap-content wow fadeIn">
	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$args = array(
			'id_form' => 'commentForm',
			'fields' => apply_filters(
				'comment_form_default_fields', array(
					
					'comment_field' => '<div class="row">
								<div class="col-sm-6">
									<textarea class="rst-page-input" id="comment" name="comment" placeholder="Add your comment" cols="30" rows="10" aria-required="true"></textarea>
								</div>',
					'author' => '<div class="col-sm-6">	
									<div class="field-group ">
										<input class="rst-page-input" id="author" name="author" placeholder="Full Name" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="Name" ' . $aria_req . ' />
									</div>
								 </div>',
					'email' => ' <div class="col-sm-6">
									<div class="field-group ">
										<input class="rst-page-input" id="email" name="email" placeholder="Email Address" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" placeholder="Your email" ' . $aria_req . ' />
									</div>
								 </div>',
					'url' => '   <div class="col-sm-6">
									 <div class="field-group ">
										 <input class="rst-page-input" id="url" name="url" placeholder="Your Website" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" placeholder="Site"/>
									 </div>
								 </div>
						     </div>'
				)
			),
			'comment_field' => '',
			'comment_notes_after'  => '',
			'comment_notes_before' => '',
			'title_reply' => '<span>Leave a Reply</span>',
			'label_submit' => 'Send'
		);
	?>
	<?php 
		if( is_user_logged_in() ) { 
			$args['comment_field'] = '<textarea class="rst-page-input" id="comment" name="comment" placeholder="Add your comment" cols="30" rows="10" aria-required="true"></textarea>';
		}
	?>
	<?php comment_form($args, get_the_ID()); ?>
</div>
