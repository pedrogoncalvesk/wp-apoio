<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package rst
 */

get_header(); ?>
	
		<!-- Main page -->
		<div class="rst-main-page" id="rst-404-page">
		
			<div class="rst-blog-container">
				<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo esc_url(get_theme_mod('rst_logo')) ?>" alt="" /></a>
				<h1><?php echo get_theme_mod('title_404') ? get_theme_mod('title_404') : '404' ?></h1>
				<p><?php echo get_theme_mod('subtitle_404') ? get_theme_mod('subtitle_404') : 'Page not found' ?></p>
				<a href="<?php echo esc_url(home_url('/')) ?>" class="btn  btn-sm"><?php rst_the_translate('main page','translation_main_page') ?></a>
			</div>
			<!-- Blog container -->
			
		</div>
		<!-- Main page -->
	
<?php get_footer(); ?>