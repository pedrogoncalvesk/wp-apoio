<?php
	$size_image = array('width'=>370,'height'=>298);
?>
<?php if( get_post_thumbnail_id(get_the_ID()) ) { ?>
	
	<?php if( is_single() ) { ?>
	<div class="rst-thumbnail">
		<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) ?>" class="img-responsive" alt="<?php the_title() ?>" /> <!-- Featured Img -->
	</div>
	<?php } else { ?>
	<div class="rst-thumbnail">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php echo bfi_thumb(wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())),$size_image) ?>" class="img-responsive" alt="<?php the_title() ?>" /> <!-- Featured Img -->
		</a>
	</div>
	<?php } ?>
	
<?php } ?>