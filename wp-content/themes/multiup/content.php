<?php
/**
 * @package rst
 */
?>
<?php
	global $rst_blog, $post;
	$class = '';
	if ( !isset($rst_blog['disable_animate']) ) $rst_blog['disable_animate'] = false;
	if( ! has_post_thumbnail() ) {
		$class .= ' rst-not-thumbnail';
	}
	else {
		$class .= ' rst-has-thumbnail';
	}
	if( ! $rst_blog['disable_animate'] ) {
		$class .= ' wow slideUp';
	}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class) ?> itemscope itemtype="http://schema.org/Article" >
	<?php get_template_part( 'thumbnail' ); ?>
	<div class="rst-blog-excerpt rst-blog-info">
		<time datetime="<?php echo mysql2date('Y-m-d', $post->post_date) ?>" itemprop="dateCreated" ><?php echo mysql2date('d F Y', $post->post_date) ?></time>
		<p class="rst-blog-title"><a href="<?php the_permalink(); ?>" itemscope itemprop="name"><?php the_title(); ?></a></p>
		<?php echo get_excerpt_by_id($post,isset($rst_blog['excerpt_length']) ? absint($rst_blog['excerpt_length']) : 0 ); ?>
		<ul>
			<?php if( ! get_theme_mod('disable_likepost') ) { ?>
			<li><?php echo rst_render_like($post->ID) ?></li>
			<?php } ?>
			<li><a href="<?php the_permalink(); ?>#comments"><i class="fa fa-comment-o"></i> <span itemprop="interactionCount"><?php comments_number('0', '1', '%');?></span></a></li>
			<li>
				<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
					<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
					<span><?php the_author()?></span>
				</a>
			</li>
		</ul>
	</div>	
	<div class="clear"></div>
</article>

			