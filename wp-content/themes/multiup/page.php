<?php
/**
 * The template for displaying all page.
 *
 * @package Multiup
 */

get_header(); ?>

			<?php get_template_part( 'banner' ); ?>
			
		</div>
	</header>
	<!-- Header -->

	<!-- Main page -->
	<div class="rst-main-page wpb_row" id="rst-contact-page">

	<?php if ( have_posts() ) : ?>

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php
				/* Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', 'page' );
			?>

		<?php endwhile; ?>

		
		<?php rst_paging_nav(); ?>

	<?php else : ?>

		<?php get_template_part( 'content', 'none' ); ?>

	<?php endif; ?>
	
	</div>

<?php get_footer(); ?>