<?php
/**
 * The template for displaying coming page.
 *
 * @package rst
 */
 

get_header(); ?>
	
		<!-- Main page -->
		<div class="rst-main-page" id="rst-coming-page">
		
			<!-- Container -->
			<div class="container">
				<?php if( get_theme_mod('rst_logo') ) { ?>
				<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo esc_url(get_theme_mod('rst_logo')) ?>" alt="" /></a>
				<?php } ?>
				<div class="rst-coming-countdown" data-date="<?php echo get_theme_mod('deadline_coming') ?>"></div>
				<div class="rst-coming-subscribe">
					<?php
						echo do_shortcode('[wysija_form id="1"]');
					?>
				</div>
			</div>
			<!-- Container -->
		
		</div>
		<!-- Main page -->
		
<?php get_footer(); ?>