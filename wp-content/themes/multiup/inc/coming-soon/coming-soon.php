<?php
$check_coming_soon = get_theme_mod('rst_check_coming_soon');

if( $check_coming_soon ){
	
	function rst_render_comingsoon_page() {
		
		// Return if a login page
		if(preg_match("/login|admin|dashboard|account/i",$_SERVER['REQUEST_URI']) > 0){
			return false;
		}

		if ( !is_user_logged_in() && ! is_admin() ) {
			$file = get_template_directory().'/inc/coming-soon/template-coming-soon.php';
			include($file);
			exit;
		}
		
	}
	add_action('template_redirect', 'rst_render_comingsoon_page',99);
	
	
	function rst_admin_bar_menu() {
		global $wp_admin_bar;
		/* Add the main siteadmin menu item */
			$wp_admin_bar->add_menu( array(
				'id'     => 'debug-bar',
				'href' => admin_url().'options-general.php?page=seedprod_coming_soon',
				'parent' => 'top-secondary',
				'title'  => apply_filters( 'debug_bar_title', __('Coming Soon Mode Active', 'ultimate-coming-soon-page') ),
				'meta'   => array( 'class' => 'ucsp-mode-active' ),
			) );
	}
	add_action( 'admin_bar_menu', 'rst_admin_bar_menu', 9000 );
}