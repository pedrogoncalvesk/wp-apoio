<?php
class rst_contacthour_widget extends WP_Widget {
	
	function rst_contacthour_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget_contacthour', 'description' => 'A widget that show information in footer' );

		/* Create the widget. */
		parent::__construct( 'rst-contacthour-widget', 'Laza - Contact Hour', $widget_ops);	 
	}
	 
	function widget($args, $instance) {
		$html = '';
		$html .= '<div class="rst-happy-hours">';
		
		$html .= $args['before_widget'];
		if( !empty($instance['title']) ) {
			$html .= $args['before_title'];
				$html .= $instance['title'];
			$html .= $args['after_title'];
		}
		
		if( !empty($instance['infohour']) ){
			
			
			$html .= '<ul>';
				foreach ($instance['infohour'] as $item) {
					$html .= '<li><span>'.$item['day'].'</span><span>'.$item['hour'].'</span></li>';
				}
				
			$html .= '</ul>';
		}
		
		if( !empty($instance['description']) ){
			$html .= $instance['description'];
		}
		
		$html .= '</div>';
		
		$html .= $args['after_widget'];
		echo force_balance_tags($html);
	}
 
	function update($new_instance, $old_instance) {
		return $new_instance;		
	}
 
	function form($instance) {
		?><br/>
		<label>Title:
		<br/><br/>
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>" style="width:100%" /></label>
		<br/><br/>
		<label>Details:
		<br/><br/>
		<?php
			rs::repeater(array(
			   'name' => $this->get_field_name( 'infohour' ), 
			   'type' => 'repeater',
			   'layout' => 'row',
			   'add_row_text' => null,
			   'min_rows' => 1,
			   'max_rows' => 999,
			   'sorting' => true,
			   'controls' => array(
				  array(
					 'name' => 'day', 
					 'label' => 'Day',
					 'type'  => 'text',
				  ),
				  array(
					 'name' => 'hour', 
					 'label' => 'Hour',
					 'type'  => 'text',
				  )
			   ),
			   'value' => isset($instance['infohour']) ? $instance['infohour'] : ''
			));
		?>
		
		<?php	
	}
	
}

add_action( 'widgets_init', 'create_contacthour_widget' );

function create_contacthour_widget(){
	return register_widget("rst_contacthour_widget");
}