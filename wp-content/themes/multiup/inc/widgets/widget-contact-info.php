<?php
class rst_contactinfo_widget extends WP_Widget {
	
	function rst_contactinfo_widget() {
		/* Widget settings. */
		if(!isset($widget_ops)) $widget_ops = array( 'classname' => 'widget_contactinfo', 'description' => 'A widget that show information in footer' );

		/* Create the widget. */
		parent::__construct( 'rst-contactinfo-widget', 'Laza - Contact Info', $widget_ops);	
	}
	 
	function widget($args, $instance) {
		$html = '';
		$html .= '<div class="rst-address">';
		
		$html .= $args['before_widget'];
		if( !empty($instance['country']) ) {
			$html .= $args['before_title'];
				$html .= $instance['country'];
			$html .= $args['after_title'];
		}
		
		if( !empty($instance['address']) ){
			$html .= '<p>'. $instance['address'] .'</p>';
		}
		if( !empty($instance['phone']) ){
			$html .= '<p><a href="tel:'.$instance['phone'].'">'. $instance['phone'] .'</a></p>';
		}
		if( !empty($instance['email']) ){
			$html .= '<p><a href="mailto:'.$instance['email'].'">'. $instance['email'] .'</a></p>';
		}
		if( !empty($instance['description']) ){
			$html .= $instance['description'];
		}
		
		
		
		$html .= '</div>';
		
		$html .= $args['after_widget'];
		echo force_balance_tags($html);
	}
 
	function update($new_instance, $old_instance) {
		return $new_instance;		
	}
 
	function form($instance) {
		?><br/>
		<label>Country:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'country' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'country' )); ?>" value="<?php echo isset($instance['country']) ? esc_attr($instance['country']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		<label>Address:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'address' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'address' )); ?>" value="<?php echo isset($instance['address']) ? esc_attr($instance['address']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		<label>Phone:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'phone' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'phone' )); ?>" value="<?php echo isset($instance['phone']) ? esc_attr($instance['phone']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		<label>Email:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'email' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'email' )); ?>" value="<?php echo isset($instance['email']) ? esc_attr($instance['email']) : ''; ?>" style="width:100%" /></label>
		
		<?php	
	}
	
}

add_action( 'widgets_init', 'create_contactinfo_widget' );

function create_contactinfo_widget(){
	return register_widget("rst_contactinfo_widget");
}