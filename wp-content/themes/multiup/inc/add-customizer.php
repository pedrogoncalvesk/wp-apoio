<?php
	// type use: text, checkbox, radio, select, dropdown-pages, textarea, color, gallery, image, group
	
	
	// Favicon
	rs::addCustomizeTab(array(
		'title' => 'Favicon', 
		'name' => 'rst_customize_favicon',
		'priority' => 20,
		'controls' => array(
			array(
				'label' 		=> 'Favicon',
				'type' 			=> 'image',
				'description'	=> '16x16 px.',
				'name' 			=> 'favicon'
			),
			array(
				'label' 		=> 'Apple iPad Retina Icon',
				'type' 			=> 'image',
				'description'	=> '144x144 px.',
				'name' 			=> 'favicon_ipad_retina'
			),
			array(
				'label' 		=> 'Apple iPad Icon',
				'type' 			=> 'image',
				'description'	=> '75x75 px.',
				'name' 			=> 'favicon_ipad'
			),
			array(
				'label' 		=> 'Apple iPhone Icon',
				'type' 			=> 'image',
				'description'	=> '57x57 px.',
				'name' 			=> 'favicon_iphone'
			)
		)
	) );
	
	// Theme Color
	rs::addCustomizeTab(array(
		'title' => 'Theme Color', 
		'name' => 'rst_customize_theme_color',
		'priority' => 21,
		'controls' => array(
			array(
				'label' 		=> 'Theme Color',
				'type' 			=> 'color',
				'default_value' => '#2ecc71',
				'name' 			=> 'rst_theme_color',
				'css'			=> '
					.rst-header-menu nav li li:hover,
					.owl-theme .owl-controls .owl-page.active span, 
					.owl-theme .owl-controls.clickable .owl-page:hover span,
					.rst-page li.active a,
					.rst-page li a:hover,
					.rst-homewelcome-tabs .rst-video a,
					.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar,
					.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar,
					.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar,
					.rst-home2 .rst-whoweare li i:hover,
					.rst-index-banner-content .btn:hover,
					.rst-page-shortdescription.rst-dark  .btn:hover,
					.col-xs-3.rst-footer-socials a:hover,
					.rst-box-info-2:hover i,
					.rst-boxinfo-style-1:hover span.rst-icon,
					.btn,
					.btn:hover,
					.btn2,
					#submit,
					.rst-page span,
					.progress-bar-success,
					.rst-video a ,
					.rst-style i:hover,
					.widget.widget_calendar table th,
					.widget.widget_calendar table tbody td#today,	
					.widget.widget_tag_cloud .tagcloud a:hover,
					#rst-coming-page input[type="submit"] 
					{
					  background-color: $value;
					  -webkit-transition: background 0.3s ease-in-out;
					  -moz-transition: background 0.3s ease-in-out;
					  -ms-transition: background 0.3s ease-in-out;
					  -o-transition: background 0.3s ease-in-out;
					  transition: background 0.3s ease-in-out;
					}
					.rst-index-banner-content .btn:hover,
					.rst-page-shortdescription.rst-dark  .btn:hover,
					.btn,
					.btn:hover,
					.btn2,
					.widget.widget_tag_cloud .tagcloud a:hover 
					{
					  border-color: $value;
					}
					.menu-item-has-children:after {
					  border-top-color: $value;
					}
					.rst-address a:hover,
					.rst-address p:last-of-type a:hover,
					.rst-contact-box i,
					.rst-contact-box > a:hover,
					.rst-labels:after,
					.rst-responsive-icon i,
					.rst-whoweare li i,
					.rst-detail-pack > ul  li p i,
					.rst-homewelcome-tabs .tab-pane ul li i,
					.rst-testimonial p i,
					#rst-work .rst-img > p,
					.rst-funfacts i,
					.rst-testimonial-slider .owl-item div i,
					.rst-blog .rst-blog-excerpt.rst-blog-info .rst-blog-title a:hover,
					.rst-uptop i,
					.rst-talkbox > p:after,
					.rst-talkbox > p:before,
					.rst-author > p:before,
					.rst-testimonial-slider div p:before,
					.rst-testimonial >p:after,
					.rst-testimonial  >p:before,
					.rst-work .rst-img p,
					.rst-boxinfo-style-2 i,
					.rst-box-info-2 i,
					.rst-list-option li i,
					.rst-blog-title a:hover,
					.widget.widget_archive a:hover,
					.widget.widget_categories a:hover,
					.widget.widget_pages li a:hover,
					.widget.widget_meta li a:hover,
					.widget.widget_recent_comments li a,
					.widget.widget_rss li a
					{
					  color: $value;
					  -webkit-transition: color 0.3s ease-in-out;
					  -moz-transition: color 0.3s ease-in-out;
					  -ms-transition: color 0.3s ease-in-out;
					  -o-transition: color 0.3s ease-in-out;
					  transition: color 0.3s ease-in-out;
					}
				'
			)
		)
	) );
	
	// General
	// rs::addCustomizePanel(array(
		// 'title' => 'General', 
		// 'name' => 'general',
	// ));
	rs::addCustomizeTab(array(
		'title' => 'General', 
		// 'panel'	=> 'general',
		'name' => 'rst_customize_general',
		'priority' => 22,
		'controls' => array(
			array(
				'label' 		=> 'Disable Like Single',
				'type' 			=> 'checkbox',
				'name' 			=> 'disable_likepost'
			),
		)
	));
	
	
	//Background Body
	rs::addCustomizeTab(array(
		'title' => 'Background Body', 
		'name' => 'rst_customize_bg_body',
		'priority' => 23,
		'controls' => array(
			array(
				'label'   => 'Content Background',
				'type'    => 'rsbackground',
				'name'    => 'bg_content',
				'css_selector' => '.rst-main-page',
				'default_value' => array(
					'background-color' => '#ffffff'
				)
			),
		)
	));
		
	//Fonts
	rs::addCustomizeTab(array(
		'title' => 'Fonts', 
		'name' => 'rst_customize_fonts',
		'priority' => 24,
		'controls' => array(
			array(
				'label' 		=> 'Body',
				'type'			=> 'font',
				'name'			=> 'font_body',
				'css_selector'	=> 'body',
				'default_value'	=> array(
					'font-family' => 'Open Sans',
					'font-size'	=> '14px',
					'font-weight' => '400'
				)
			),
			array(
				'label' 		=> 'H1',
				'type'			=> 'font',
				'name'			=> 'font_h1',
				'css_selector'	=> 'h1',
				'default_value'	=> array(
					'font-size'	=> '45px',
					'line-height' => '58px',
					'font-weight' => '400',
				)
			),
			array(
				'label' 		=> 'H2',
				'type'			=> 'font',
				'name'			=> 'font_h2',
				'css_selector'	=> 'h2',
				'default_value'	=> array(
					'font-size'	=> '22px',
					'line-height' => '26px',
					'font-weight' => '600',
				)
			),
			array(
				'label' 		=> 'H3',
				'type'			=> 'font',
				'name'			=> 'font_h3',
				'css_selector'	=> 'h3',
				'default_value'	=> array(
					'font-size'	=> '20px',
					'line-height' => '24px',
					'font-weight' => '600',
				)
			),
			array(
				'label' 		=> 'H4',
				'type'			=> 'font',
				'name'			=> 'font_h4',
				'css_selector'	=> 'h4',
				'default_value'	=> array(
					'font-size'	=> '18px',
					'line-height' => '20px',
					'font-weight' => '600',
				)
			),
			array(
				'label' 		=> 'H5',
				'type'			=> 'font',
				'name'			=> 'font_h5',
				'css_selector'	=> 'h5',
				'default_value'	=> array(
					'font-size'	=> '16px',
					'line-height' => '18px',
					'font-weight' => '600',
				)
			),
			array(
				'label' 		=> 'H6',
				'type'			=> 'font',
				'name'			=> 'font_h6',
				'css_selector'	=> 'h6',
				'default_value'	=> array(
					'font-size'	=> '14px',
					'line-height' => '16px',
					'font-weight' => '600',
				)
			),
		)
	));
	
	
	
	// Header
	rs::addCustomizePanel(array(
		'title' => 'Header', 
		'name' => 'header',
		'priority' => 25,
	) );
	
	// Header > Header Setting
	rs::addCustomizeTab(array(
		'title' => 'Header Setting', 
		'name' => 'rst_customize_header_setting',
		'panel'	=> 'header',
		'controls' => array(
			array(
				'label' 		=> 'Background Header on Stick',
				'type' 			=> 'color',
				'name' 			=> 'header_bg_color',
				'default_value'	=> '#223040',
				'css'			=> '.rst-header-menu.active { background-color: $value }'
			),
			array(
				'label' 		=> 'Banner Title Default',
				'type' 			=> 'text',
				'name' 			=> 'header_title'
			),
			array(
				'label' 		=> 'Banner Background',
				'type' 			=> 'rsbackground',
				'name' 			=> 'bg_banner',
				'css_selector' => '#rst-header .rst-blog-banner',
				'default_value' => array(
				 'background-color' => '#2a3f50'
				)
			),
			array(
				'label' 		=> 'Background Transition (percen)',
				'type' 			=> 'text',
				'name' 			=> 'header_banner_transition',
				'default_value'	=> '0.8',
				'description'	=> '',
				'css'			=> '.rst-blog-banner:after { opacity: $value }'
			),
			array(
				'label' 		=> 'Hide Search',
				'type' 			=> 'checkbox',
				'name' 			=> 'header_hide_search'
			),
		)
	) );
	
	
	// Header > Logo
	rs::addCustomizeTab(array(
		'title' => 'Logo', 
		'name' => 'rst_customize_logo',
		'panel'	=> 'header',
		'controls' => array(
			array(
				'label' 		=> 'Header Logo',
				'type' 			=> 'image',
				'name' 			=> 'rst_logo'
			),
			array(
				'label' 		=> 'Max Width',
				'type' 			=> 'text',
				'name' 			=> 'rst_logo_max_width',
				'css'			=> '.rst-header-menu .container > a img { max-width: $value }'
			),
			array(
				'label' 		=> 'Max Height',
				'type' 			=> 'text',
				'name' 			=> 'rst_logo_max_height',
				'css'			=> '.rst-header-menu .container > a img { max-height: $value }'
			),
			array(
				'label' 		=> 'Padding Top',
				'type' 			=> 'text',
				'name' 			=> 'rst_logo_padding_top',
				'default_value'	=> '',
				'css'			=> '.rst-header-menu .container > a img { padding-top: $value }'
			),
			array(
				'label' 		=> 'Padding Bottom',
				'type' 			=> 'text',
				'name' 			=> 'rst_logo_padding_bottom',
				'default_value'	=> '',
				'css'			=> '.rst-header-menu .container > a img { padding-bottom: $value }'
			),
			
		)
	) );
	
	// Header > Menu
	rs::addCustomizeTab(array(
		'title' => 'Menu Setting', 
		'name' => 'rst_customize_menu_setting',
		'panel'	=> 'header',
		'controls' => array(
			
			array(
				'label' 		=> 'Menu Color',
				'type' 			=> 'color',
				'name' 			=> 'header_menu_color',
				'default_value'	=> '#fff',
				'css'			=> '.rst-header-search i,.rst-header-menu nav li a { color: $value }'
			),
			array(
				'label' 		=> 'Menu Hover Color',
				'type' 			=> 'color',
				'name' 			=> 'header_menu_hover_color',
				'default_value'	=> '#70859b',
				'css'			=> '.rst-header-menu nav > ul > li > a:hover, .col-xs-6.rst-footer-menu a:hover { color: $value }'
			),
			array(
				'label' 		=> 'Sub Menu Color',
				'type' 			=> 'color',
				'name' 			=> 'header_submenu_color',
				'default_value'	=> '#223040',
				'css'			=> '.rst-header-menu nav li li a { color: $value }'
			),
			array(
				'label' 		=> 'Sub Menu Background',
				'type' 			=> 'color',
				'name' 			=> 'header_submenu_bg',
				'default_value'	=> '#70859b',
				'css'			=> '.rst-header-menu nav ul ul { background-color: $value }'
			),
		)
	) );
	
	// Footer
	rs::addCustomizeTab(array(
		'title' => 'Footer', 
		'name' => 'rst_customize_footer',
		'priority' => 26,
		'controls' => array(
			array(
				'label' 		=> 'Footer Top Columns',
				'type' 			=> 'select',
				'name'			=> 'footer_column',
				'default_value'	=> 4,
				'items'			=> array(
					'4' => '4 Columns',
					'3' => '3 Columns',
					'2' => '2 Columns',
					'1' => '1 Column',
				)
			),
			array(
				'label' 		=> 'Logo',
				'type' 			=> 'image',
				'name'			=> 'footer_logo'
			),
			array(
				'label' 		=> 'Copyright',
				'type' 			=> 'textarea',
				'default_value'	=> '&copy; 2015 multiup. All right reserved.',
				'name'			=> 'footer_copyright'
			),
			array(
				'label'   => 'Footer Background',
				'type'    => 'rsbackground',
				'name'    => 'bg_footer',
				'css_selector' => '.rst-footer',
				'default_value' => array(
					'background-color' => '#2a3f50'
				)
			),
			array(
				'label' 		=> 'Hide social',
				'type' 			=> 'checkbox',
				'default_value'	=> '',
				'name'			=> 'footer_hide_social'
			),
			array(
				'label' 		=> 'Show Button BackToTop',
				'type' 			=> 'checkbox',
				'default_value'	=> '',
				'name'			=> 'footer_button_back'
			),
		)
	) );
	
	
	// Category
	rs::addCustomizeTab(array(
		'title' => 'Category & Author Page', 
		'name' => 'rst_customize_category',
		'priority' => 27,
		'controls' => array(
			array(
				'label' 		=> 'Posts Per Page',
				'type' 			=> 'text',
				'default_value'	=> 10,
				'name'			=> 'rst_cat_numberpost'
			),
			array(
				'label' 		=> 'Excerpt Length',
				'type' 			=> 'text',
				'default_value'	=> 30,
				'name'			=> 'rst_cat_excerpt_length'
			),
			array(
				'label' 		=> 'Disable Animate',
				'type' 			=> 'checkbox',
				'name' 			=> 'cat_disable_animate'
			),
			array(
				'label' 		=> 'Hide sidebar',
				'type' 			=> 'checkbox',
				'name' 			=> 'hide_cat_sidebar'
			),
			array(
				'label' 		=> 'Hide pagenavi',
				'type' 			=> 'checkbox',
				'name' 			=> 'cat_hide_pagenavi'
			)
		)
	) );
	
	// Search
	rs::addCustomizeTab(array(
		'title' => 'Search', 
		'name' => 'rst_customize_search',
		'priority' => 28,
		'controls' => array(
			array(
				'label' 		=> 'Posts Per Page',
				'type' 			=> 'text',
				'default_value'	=> 10,
				'name'			=> 'rst_search_numberpost'
			),
			array(
				'label' 		=> 'Excerpt Length',
				'type' 			=> 'text',
				'default_value'	=> 30,
				'name'			=> 'rst_search_excerpt_length'
			),
			array(
				'label' 		=> 'Disable Animate',
				'type' 			=> 'checkbox',
				'name' 			=> 'search_disable_animate'
			),
			array(
				'label' 		=> 'Hide sidebar',
				'type' 			=> 'checkbox',
				'name' 			=> 'hide_search_sidebar'
			),
			array(
				'label' 		=> 'Hide pagenavi',
				'type' 			=> 'checkbox',
				'name' 			=> 'search_hide_pagenavi'
			)
		)
	) );
	
	
	// Post
	rs::addCustomizeTab(array(
		'title' => 'Post', 
		'name' => 'rst_customize_post',
		'priority' => 29,
		'controls' => array(
			array(
				'label' 		=> 'Hide Sidebar',
				'type' 			=> 'checkbox',
				'name' 			=> 'single_hide_sidebar'
			),
			array(
				'label' 		=> 'Hide Thumbnail',
				'type' 			=> 'checkbox',
				'name' 			=> 'singe_hide_thumbnail'
			),
			array(
				'label' 		=> 'Hide Date',
				'type' 			=> 'checkbox',
				'name' 			=> 'singe_hide_date'
			),
			array(
				'label' 		=> 'Hide Comment Count',
				'type' 			=> 'checkbox',
				'name' 			=> 'singe_hide_comment_count'
			),
			array(
				'label' 		=> 'Hide Author',
				'type' 			=> 'checkbox',
				'name' 			=> 'singe_hide_author'
			),
			array(
				'label' 		=> 'Hide Comment Box',
				'type' 			=> 'checkbox',
				'name' 			=> 'singe_hide_comment'
			),
		)
	));
	
	// Page
	rs::addCustomizeTab(array(
		'title' => 'Page', 
		'name' => 'rst_customize_page',
		'priority' => 30,
		'controls' => array(
			array(
				'label' 		=> 'Hide Comment Box',
				'type' 			=> 'checkbox',
				'name' 			=> 'page_hide_comment'
			),
		)
	));
	
	// 404
	rs::addCustomizeTab(array(
		'title' => '404', 
		'name' => 'rst_customize_404',
		'priority' => 31,
		'controls' => array(
			array(
				'label' 		=> 'Title',
				'type' 			=> 'text',
				'name'			=> 'title_404',
				'default_value'	=> '404'
			),
			array(
				'label' 		=> 'Sub Title',
				'type' 			=> 'text',
				'name'			=> 'subtitle_404',
				'default_value'	=> 'Page not found'
			),
			array(
				'label'   => 'Content Background',
				'type'    => 'rsbackground',
				'name'    => 'bg_content_404',
				'css_selector' => '#rst-404-page',
				'default_value' => array(
					'background-color' => '#ffffff'
				)
			),
		)
	) );
	
	
	// Seo
	rs::addCustomizeTab(array(
		'title' => 'Seo', 
		'name' => 'rst_customize_seo',
		'priority' => 32,
		'controls' => array(
			array(
				'label' 		=> 'Meta Description',
				'type' 			=> 'textarea',
				'description'	=> 'Enter your website meta description for SEO.',
				'name' 			=> 'des_seo'
			),
			array(
				'label' 		=> 'Meta Keywords',
				'type' 			=> 'textarea',
				'description'	=> 'Enter your keywords here separated by a comma.',
				'name' 			=> 'keywords_seo'
			),
			array(
				'label' 		=> 'Meta Author',
				'type' 			=> 'textarea',
				'description'	=> '',
				'name' 			=> 'author_seo'
			),
		)
	) );
	
	// Social Network
	rs::addCustomizeTab(array(
		'title' => 'Social Network', 
		'name' => 'rst_customize_social',
		'priority' => 33,
		'controls' => array(
			array(
				'label' 		=> 'Facebook',
				'type' 			=> 'text',
				'name' 			=> 'social_facebook'
			),
			array(
				'label' 		=> 'Google plus',
				'type' 			=> 'text',
				'name' 			=> 'social_google'
			),
			array(
				'label' 		=> 'Twitter',
				'type' 			=> 'text',
				'name' 			=> 'social_twitter'
			),
			array(
				'label' 		=> 'Tumblr',
				'type' 			=> 'text',
				'name' 			=> 'social_tumblr'
			),
			array(
				'label' 		=> 'Instagram',
				'type' 			=> 'text',
				'name' 			=> 'social_instagram'
			),
			array(
				'label' 		=> 'Youtube',
				'type' 			=> 'text',
				'name' 			=> 'social_youtube'
			),
			array(
				'label' 		=> 'Linkedin',
				'type' 			=> 'text',
				'name' 			=> 'social_linkedin'
			),
			array(
				'label' 		=> 'Flickr',
				'type' 			=> 'text',
				'name' 			=> 'social_flickr'
			),
			array(
				'label' 		=> 'Vimeo',
				'type' 			=> 'text',
				'name' 			=> 'social_vimeo'
			),
			array(
				'label' 		=> 'Pinterest',
				'type' 			=> 'text',
				'name' 			=> 'social_pinterest'
			),
			array(
				'label' 		=> 'Dribbble',
				'type' 			=> 'text',
				'name' 			=> 'social_dribbble'
			),
			array(
				'label' 		=> 'Digg',
				'type' 			=> 'text',
				'name' 			=> 'social_digg'
			),
			array(
				'label' 		=> 'Skype',
				'type' 			=> 'text',
				'name' 			=> 'social_skype'
			),
			array(
				'label' 		=> 'Deviantart',
				'type' 			=> 'text',
				'name' 			=> 'social_deviantart'
			),
			array(
				'label' 		=> 'Yahoo',
				'type' 			=> 'text',
				'name' 			=> 'social_yahoo'
			),
			array(
				'label' 		=> 'Reddit',
				'type' 			=> 'text',
				'name' 			=> 'social_reddit'
			)
		)
	) );
	
	 
	// Coming
	rs::addCustomizeTab(array(
		'title' => 'Coming Soon',
		'name' => 'rst_customize_coming',
		'priority' => 34,
		'controls' => array(
			array(
				'label' 		=> 'Enable Coming Soon',
				'type' 			=> 'checkbox',
				'name' 			=> 'rst_check_coming_soon'
			),
			array(
				'label' 		=> 'Deadline',
				'type' 			=> 'text',
				'name'			=> 'deadline_coming',
				'description'	=> 'Format: YYYY/MM/DD',
				'default_value'	=> '2015/9/15'
			),
			array(
				'label'   => 'Content Background',
				'type'    => 'rsbackground',
				'name'    => 'bg_content_coming',
				'css_selector' => '#rst-coming-page',
				'default_value' => array(
					'background-color' => '#ffffff'
				)
			),
		)
	) );
	
	// Translations
	rs::addCustomizeTab(array(
		'title' => 'Translations / ReName Text', 
		'name' => 'rst_customize_translations',
		'priority' => 35,
		'controls' => array(
			array(
				'label' 		=> 'Continue reading',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_continue_reading'
			),
			array(
				'label' 		=> 'Load more posts',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_loadmore'
			),
			array(
				'label' 		=> 'Go to Home Page',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_go_to_home'
			),
			array(
				'label' 		=> 'Search',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_search'
			),
			array(
				'label' 		=> 'Search resuilt for',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_search_resuilt'
			),
			array(
				'label' 		=> 'Search for',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_search_for'
			),
			array(
				'label' 		=> 'If you\'re not happy with the results, please do another search',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_search_not_happy'
			),
			array(
				'label' 		=> 'Archives',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_archives'
			),
			array(
				'label' 		=> 'Yearly Archives',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'yearly_archives'
			),
			array(
				'label' 		=> 'Monthly Archives',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'monthly_archives'
			),
			array(
				'label' 		=> 'Daily Archives',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'daily_archives'
			),
			array(
				'label' 		=> 'Browsing Tag',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'browsing_tag'
			),
			array(
				'label' 		=> 'Category',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_category'
			),
			array(
				'label' 		=> 'Author',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_author'
			),
			array(
				'label' 		=> 'Main Page',
				'type' 			=> 'text',
				'default_value'	=> '',
				'name'			=> 'translation_main_page'
			)
		)
	) );
?>