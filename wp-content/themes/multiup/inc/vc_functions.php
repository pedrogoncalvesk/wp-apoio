<?php
$attributes = array(
	"type" => "dropdown",
      "heading" => __('Container', 'rst'),
      "param_name" => "select_width",
      "value" => array(
                        __("None", 'rst') => '1',
                        __("Container", 'rst') => '2',
                        __("Small Container", 'rst') => '3',
                      ),
      "description" => __("Select width of container", 'rst'),
);
vc_add_param('vc_row', $attributes);