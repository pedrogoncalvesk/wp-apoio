<?php

function rst_rating_scripts() {
	wp_register_script( 'jquery-rst-like', esc_url( get_template_directory_uri()) . '/inc/like/js/like.js', array('jquery'), '', true );
	wp_localize_script('jquery-rst-like', 'ajax', array('url' => admin_url('admin-ajax.php')));
	wp_enqueue_script('jquery-rst-like');
}
add_action( 'wp_enqueue_scripts', 'rst_rating_scripts' );

add_action('wp_ajax_rst_ajax_like', 'rst_ajax_like_action');
add_action('wp_ajax_nopriv_rst_ajax_like', 'rst_ajax_like_action');

function rst_ajax_like_action() { 
	if( empty($_POST['postID']) && empty($_POST['point']) ) return false;
	$postID = absint($_POST['postID']);
	rst_set_post_like($postID);
	echo rst_render_like($postID,true);
	exit;
}

function rst_render_like($postID=0,$ajax=false) {
	$html = '';
	if( $postID == 0 ) $postID = get_the_ID();
	$count_key = 'rst_post_like_count';
	$count = get_post_meta($postID, $count_key, true);
	if( $ajax == false && !(isset($_COOKIE['rst_cookie_rating_'.$postID]) && $_COOKIE['rst_cookie_rating_'.$postID] == 1) ) {
		$html = '<span class="wrap-like"><a href="#" data-id="'. $postID .'" class="rst-like"><span><i class="fa fa-heart-o"></i> '. rst_get_post_like($postID) .'</span></a></span>';
	}
	else {
		$html = '<span class="wrap-like"><span class="rst-like"><span><i class="fa fa-heart-o"></i> '. rst_get_post_like($postID) .'</span></span></span>';
	}
	return $html;
}

function rst_set_post_like($postID) {
	if( !isset($postID) ) return;
	$count_key = 'rst_post_like_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}


function rst_get_post_like($postID){
	$count_key = 'rst_post_like_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count=='' || $count <= 0 ){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return 0;
	}
	if( $count != 0 )
		return $count;
	return 0;
}