<?php
global $RS;

// Banner Setting

$RS->metabox(array(
	'name' => 'banner_setting',
	'title' => 'Banner Setting',
	'rules' => array( 
		'post_type' => 'page|post|portfolio'
	),
	'controls' => array(
		array(
		   'name'      => 'rst_header_layout',
		   'label'	=> 'Select header layout',
		   'display' => 'inline',
		   'type'  => 'radio',
		   'default_value' => 1,
		   'items' => array(
				array(
					'value' => 1,
					'image' => get_template_directory_uri() . '/inc/css/images/banner1.jpg'
				),
				array(
					'value' => 2,
					'image' => get_template_directory_uri() . '/inc/css/images/banner2.jpg'
				),
				array(
					'value' => 3,
					'image' => get_template_directory_uri() . '/inc/css/images/slider-1.jpg'
				)
		   )
		),
		array(
			'name' => 'rst_banner_height',
			'label'=> 'Banner Height',
			'type' => 'text', 			'default_value'	=> '630',
			'conditional_logic' => array('rst_header_layout' => '1|2')
		),
		array(
			'name' => 'rst_banner_title',
			'label'=> 'Banner Title',
			'type' => 'text',
			'conditional_logic' => array('rst_header_layout' => '1|2')
		),
		array(
			'name' => 'rst_banner_revslides',
			'label'=> 'Shortcode RevSlides',
			'type' => 'text',
			'conditional_logic' => array('rst_header_layout' => 3)
		),
		array(
			'name' => 'rst_banner_subtitle',
			'label'=> 'Banner Subtitle',
			'type' => 'text',
			'conditional_logic' => array('rst_header_layout' => 2)
		),
		array(
			'name' => 'rst_header_banner_button',
			'label'=> 'Add Button',
			'type' => 'repeater',
			'layout' => 'row',
			'add_row_text' => null,
			'min_rows' => 0,
			'sorting' => true,
			'controls' => array(
				array(
					'name' => 'rst_button_text',
					'type'  => 'text',
					'label'=> 'Button text',
				),
				array(
					'name' => 'rst_button_link',
					'type'  => 'text',
					'label'=> 'Button link',
				)
			),
			'default_value' => array(),
			'value' => '',
			'conditional_logic' => array('rst_header_layout' => 2)
		),
		array(
			'name'  => 'rst_showvideo',
			'type'  => 'checkbox',
			'label'=> 'Show popup video in banner',
			'value' => array('show'),
			'items' => array('show' => 'Show' ),
			'conditional_logic' => array('rst_header_layout' => 2)
		),
		array(
			'name' => 'rst_banner_vimeolink',
			'label'=> 'Video link - only YouTube, Vimeo are supported',
			'type' => 'text',
			'description' => 'E.g. <br />Youtube: http://www.youtube.com/watch?v=GUEZCxBcM78 <br /> Vimeo: https://vimeo.com/49614838',
			'conditional_logic' => array(
				'rst_header_layout' => '2' , 
				'rst_showvideo' => 'show'
			)
		),
		array(
			'name' => 'rst_select_banner_container',
			'type' => 'select',
			'label' => 'Select container for banner',
			'items' => array(
				'container' => 'Container',
				'smallcontainer' => 'Small container'
			),
			'multiple' => false,
			'value' => 'container',
			'conditional_logic' => array('rst_header_layout' => 2)
		),
		array(
			'name' => 'rst_banner_background',
			'label'=> 'Header Background',
			'type' => 'image',
			'conditional_logic' => array('rst_header_layout' => '1|2')
		)
	)
));


// Post type Partners
$RS->metabox(array(
	'name' => 'partners_options',
	'title' => 'Partners Options',
	'rules' => array(
		'post_type' => 'partners',
	),
	'controls' => array(
		array(
			'name' => 'rst_partner_link',
			'label'=> 'Link',
			'type' => 'text',
		),
	)
));

// Post type Testimonials
$RS->metabox(array(
	'name' => 'testimonial_options',
	'title' => 'Testimonials Options',
	'rules' => array(
		'post_type' => 'testimonials',
	),
	'controls' => array(
		array(
			'name' => 'rst_testimonial_text',
			'label'=> 'Text',
			'type' => 'textarea',
		),
		array(
			'name' => 'rst_testimonial_name',
			'label'=> 'Name',
			'type' => 'text',
		),
	)
));

// //Get All Shortcode CTF7
// $aray_ctf7 = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
// $ctf7 = array(''=>'Select Contact Form');
// if( $cf7Forms = get_posts( $aray_ctf7 ) ){
	// foreach($cf7Forms as $cf7Form){
		// $ctf7[$cf7Form->ID] = '[contact-form-7 id="'.$cf7Form->ID.'" title="'.($cf7Form->post_title).'"]';
	// }
// }


// // Post Contact
// $RS->metabox(array(
	// 'name' => 'page_contact_setting',
	// 'title' => 'Page Contact',
	// 'rules' => array(
		// 'page_template' => 'template-contact.php',
	// ),
	// 'controls' => array(
		// array(
			// 'name' => 'rst_show_content_contact',
			// 'label'=> 'Show Content',
			// 'type' => 'switch',
			// 'style' => 'default',
			// 'default_value' => false
		// ),
		// array(
			// 'name' => 'rst_title_form',
			// 'label'=> 'Title Form',
			// 'type' => 'text'
		// ),
		// array(
			// 'name' => 'rst_shortcode_form',
			// 'label'=> 'Shortcode Contact Form 7',
			// 'type' => 'select',
			// 'items'	=> $ctf7
		// ),
		// array(
			// 'name' => 'rst_address',
			// 'label'=> 'Address',
			// 'type' => 'text',
			// 'description' => 'Go to http://www.latlong.net and put the name of a place, city, state, or address, or click the location on the map to get lat long coordinates<br>eg:<b>10.731688,122.5505356</b>'
		// ),
		// array(
			// 'name' => 'rst_map_zoom',
			// 'label'=> 'Zoom Map',
			// 'type' => 'text',
			// 'description' => ''
		// ),
		// array(
			// 'name' => 'rst_map_height',
			// 'label'=> 'Height Map (px)',
			// 'type' => 'text',
			// 'description' => ''
		// )
	// )
// ));

// // Post Home
// $RS->metabox(array(
	// 'name' => 'page_home_setting',
	// 'title' => 'Featured Posts',
	// 'rules' => array(
		// 'page_template' => 'template-home.php',
	// ),
	// 'controls' => array(
		// array(
			// 'name' => 'rst_home_slider',
			// 'label'=> 'Template',
			// 'type' => 'radio',
			// 'display' => 'inline',
			// 'items'	=> array(
				// array(
					// 'value' => '1',
					// 'text' => '',
					// 'image' => get_template_directory_uri() . '/inc/css/images/slider-1.jpg'
				// ),
				// array(
					// 'value' => '2',
					// 'text' => '',
					// 'image' => get_template_directory_uri() . '/inc/css/images/slider-2.jpg'
				// )
			// ),
			// 'default_value' => '1'
		// ),
		// array(
			// 'name' => 'rst_home_featured_post_hide_mobi',
			// 'label'=> 'Hidden on Mobile',
			// 'type' => 'switch',
			// 'default_value'	=> true
		// ),
		// array(
			// 'name' => 'rst_home_slider_post',
			// 'label'=> 'Select Posts',
			// 'min_rows'	=> 0,
			// 'type' => 'repeater',
			// 'add_row_text' => 'Add Post',
			// 'controls' => array(
				// array(
					// 'name' => 'post',
					// 'label' => 'Post',
					// 'type'  => 'postlist',
				// )
			// )
		// )
	// )
// ));

// // Popular Post Home
// $RS->metabox(array(
	// 'name' => 'page_home_popular_setting',
	// 'title' => 'Popular Posts',
	// 'rules' => array(
		// 'page_template' => 'template-home.php',
	// ),
	// 'controls' => array(
		// array(
			// 'name' => 'rst_home_popular_post',
			// 'label'=> 'Show Popular Posts',
			// 'type' => 'switch',
			// 'default_value' => false
		// ),
		// array(
			// 'name' => 'rst_home_popular_post_hide_mobi',
			// 'label'=> 'Hidden on Mobile',
			// 'type' => 'switch',
			// 'default_value'	=> true
		// ),
		// array(
			// 'name' => 'rst_home_popular_post_title',
			// 'label'=> 'Popular Posts Title',
			// 'type' => 'text'
		// ),
		// array(
			// 'name' => 'rst_home_popular_post_order',
			// 'label'=> 'Popular By',
			// 'type' => 'select',
			// 'items' => array(
				// 'week' => 'Week',
				// 'month' => 'Month'
			// )
		// ),
		// array(
			// 'name' => 'rst_home_popular_post_number',
			// 'label'=> 'Number Posts Show',
			// 'type' => 'text',
			// 'default_value'	=> 10
		// )
	// )
// ));