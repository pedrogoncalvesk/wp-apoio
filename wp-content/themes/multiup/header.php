<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package rst
 */
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if( get_theme_mod('des_seo') ) { ?>
<meta name="description" content="<?php echo esc_attr(get_theme_mod('des_seo')) ?>">
<?php } ?>
<?php if( get_theme_mod('keywords_seo') ) { ?>
<meta name="keywords" content="<?php echo esc_attr(get_theme_mod('keywords_seo')) ?>">
<?php } ?>
<?php if( get_theme_mod('author_seo') ) { ?>
<meta name="author" content="<?php echo esc_attr(get_theme_mod('author_seo')) ?>">
<?php } ?>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicons
================================================== -->
<?php if( get_theme_mod('favicon') ) { ?>
<link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod('favicon')) ?>" type="image/x-icon">
<?php } else { ?>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" type="image/x-icon">
<?php } ?>

<?php if( get_theme_mod('favicon_iphone') ) { ?>
<link rel="apple-touch-icon" href="<?php echo esc_url(get_theme_mod('favicon_iphone')) ?>">
<?php } else { ?>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() ?>/images/apple-touch-icon.png">
<?php } ?>

<?php if( get_theme_mod('favicon_ipad') ) { ?>
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url(get_theme_mod('favicon_ipad')) ?>">
<?php } else { ?>
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() ?>/images/apple-touch-icon-72x72.png">
<?php } ?>

<?php if( get_theme_mod('favicon_ipad_retina') ) { ?>
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url(get_theme_mod('favicon_ipad_retina')) ?>">
<?php } else { ?>
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() ?>/images/apple-touch-icon-114x114.png">
<?php } ?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Wrapper -->
	<div id="wrapper" class="wpb_row">
		<div class="top-row-contact">
			<p>LIGUE PARA NÓS: (11) 5055-1988 / (11) 5054-3834 - Atendimento de segunda à sexta, das 8h30 às 18h.
			</p>
		</div>
	
		<?php if ( ! is_404() && !( get_theme_mod('rst_check_coming_soon') && !is_user_logged_in() ) ) { ?>	
		<!-- Header -->
		<header id="rst-header" class="wpb_row">
			<?php if ( is_page() || is_single() ) { 
				$page_id = get_the_ID();
				$header_layout = rs::getField('rst_header_layout',$page_id);
				$header_height = rs::getField('rst_banner_height',$page_id) ? rs::getField('rst_banner_height',$page_id) : 630;
				// $banner_bg = rs::getField('rst_banner_background',$page_id,'image','url');
				// $style = $banner_bg ? "background-image: url('". $banner_bg ."');" : '';
			?>
				<div class="rst-blog-banner rst-page-banner bcg" 
				data-0="background-position:50% 0px;"
				data-center="background-position: 50% 0px;" 
				data-top-bottom="background-position: 50% 150px;" 
				data-bottom-top="background-position: 50% -150px;"  
				data-anchor-target="#wrapper"
				>
			
			<?php } else { ?>
				<div style="height: 630px" class="rst-blog-banner rst-page-banner bcg"
				data-0="background-position:50% 0px;"
				data-center="background-position: 50% 0px;" 
				data-top-bottom="background-position: 50% 150px;" 
				data-bottom-top="background-position: 50% -150px;"  
				data-anchor-target="#wrapper" 
				>
			<?php } ?>

				<!-- Header menu -->
				<div class="rst-header-menu">
					<div class="container">
						<?php if( get_theme_mod('rst_logo') ) { ?>
						<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo esc_url(get_theme_mod('rst_logo')) ?>" alt="" /></a>
						<?php } else { ?>
						<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo get_template_directory_uri() ?>/images/header-logo.png" alt="" /></a>
						<?php } ?>
						<nav>
							<?php if( ! get_theme_mod('header_hide_search') ) { ?>
								<a class="rst-header-search" href="#"><i class="fa fa-search"></i></a>
							<?php } ?>
							<?php 
								if ( has_nav_menu( 'header_menu' ) ) {
									wp_nav_menu(
										array(
											'theme_location' => 'header_menu',
											'menu_class'=> '',
											'container' => false
										)
									);
								}
							?>
							<div class="rst-menu-mobile">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
							<div class="clear"></div>
						</nav>
						<div class="clear"></div>
					</div>
					
					<!-- Drop search -->
					<div class="rst-dropsearch">
						<div class="container">
							<?php get_search_form(); ?>
						</div>
					</div>
					<!-- Drop search -->
					
				</div>
				<!-- Header menu -->
				
		<?php } ?>		
	