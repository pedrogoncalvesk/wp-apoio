	
	<?php if ( ! is_404() && !( get_theme_mod('rst_check_coming_soon') && !is_user_logged_in() ) ) { ?>
	
		<!-- Footer -->
		<footer class="rst-footer">
			<div class="container">								
			<?php
				$layout_footer = 4;
				if( get_theme_mod('footer_column') ) $layout_footer = get_theme_mod('footer_column');
				get_template_part( 'layout/footer', 'col-'.$layout_footer );
			?>
				
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1 rst-footer-copyright" style="text-align:center;">
						<?php if( get_theme_mod('footer_logo') ) { ?>
							<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo esc_url(get_theme_mod('footer_logo')) ?>" alt="" style="padding-top: 30px; width: 200px; float: left;" /></a>
						<?php } else { ?>
							<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo get_template_directory_uri() ?>/images/footer-logo.png" alt="" /></a>
						<?php } ?>
						
						<p style="color:#fff;margin-top: 73px;"><?php echo get_theme_mod('footer_copyright') ? get_theme_mod('footer_copyright') : '&copy; 2015 multiup. All right reserved.' ?></p>
						
					</div>
					<!-- <div class="col-xs-6 rst-footer-menu">
						<?php 
							if ( has_nav_menu( 'footer_menu' ) ) {
								wp_nav_menu(
									array(
										'theme_location' => 'footer_menu',
										'menu_class'=> '',
										'container' => false
									)
								);
							}
						?>
					</div>
					<div class="col-xs-3 rst-footer-socials">
						<?php $is_hide_social = get_theme_mod('footer_hide_social'); ?>
							<?php if( !$is_hide_social ) { ?>
							<?php get_template_part( 'content', 'social' ); ?>
						<?php } ?>
					</div> -->
					<?php if( get_theme_mod('footer_button_back') ) { ?>
					<div class="col-xs-12">
						<a class="rst-uptop" href="javascript:;"><i class="pe-7s-angle-up"></i></a>
					</div>
					<?php } ?>
				</div>
			</div>
		</footer>
		<!-- Footer -->
	
	<?php } ?>
	
	</div>
	<!-- Wrapper -->
	
	<?php wp_footer(); ?>
	
  </body>
</html>
