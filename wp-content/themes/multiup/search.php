<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rst
 */

 get_header(); ?>
 
				<!-- Header banner -->
				<div class="rst-header-banner">
					<div class="container">
						<div class="rst-banner-content">
							<div class="rst-banner-title">
								<h1><?php rst_the_translate('Search resuilt for','translation_search_resuilt') ?>: "<?php the_search_query(); ?>"</h1>
							</div>
							<div class="rst-scroll-down">
								<a href="#"><i class="fa fa-chevron-down"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- Header banner -->
				
			</div>
		</header>
		<!-- Header -->
		
		<!-- Main page -->
		<div class="rst-main-page archive-page" id="rst-blog-page">
		
			<?php
				global $rst_blog;
				$rst_blog['excerpt_length'] = get_theme_mod('rst_search_excerpt_length');
				$rst_blog['disable_animate'] = get_theme_mod('search_disable_animate');
				
				$post_per_page = get_theme_mod('rst_search_numberpost') ? get_theme_mod('rst_search_numberpost') : 10;
				$args = array(
					'post_type'			=> 'post',
					'posts_per_page' 	=> $post_per_page,
					'paged' 			=> max( get_query_var( 'paged' ), get_query_var( 'page' ))
				);
				$args = array_merge( $wp_query->query_vars, $args );
				$the_query = new WP_Query( $args );
				$wp_query = $the_query;
			?>
			
			<div class="container rst-blog-page-container">
		
				<div class="row">
				
					<div class="rst-blog-page <?php if( ! get_theme_mod('hide_search_sidebar') ) { echo 'col-sm-9';} ?>">
					
						<?php if( $the_query->have_posts() ) { ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							?>

						<?php endwhile; ?>
						<?php } else { ?>
							
							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', 'none' );
							?>
						<?php } ?>
						
						<?php if( !get_theme_mod('search_hide_pagenavi') ) { ?>
						<!-- Page navigation -->
						<div class="rst-page">
							<?php rst_the_posts_navigation(); ?>
						</div>
						<!-- Page navigation -->
						<?php } ?>
						
					</div>
			
					<div class="rst-page-has-sidebar <?php if( ! get_theme_mod('hide_search_sidebar') ) { echo 'col-sm-3';} ?>">
						<?php
							if( ! get_theme_mod('hide_search_sidebar') )
							{
								if ( is_active_sidebar( 'sidebar' ) ) {
									dynamic_sidebar( 'sidebar' );
								}
							}
						?>
					</div>
			
				</div>
				
		</div>
		<!-- Blog container -->
			
		</div>
		<!-- Main page -->
		
<?php get_footer(); ?>		