<?php
/**
 * The template for displaying all single posts.
 *
 * @package rst
 */

get_header(); ?>
			<?php get_template_part( 'banner' ); ?>
			
		</div>
	</header>
	<!-- Header -->

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part( 'content', 'single' ); ?>
		
	<?php endwhile; endif; ?>
	
<?php get_footer(); ?>