<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Multiup
 */
?>

	<?php
		the_content(); 
		
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'ublog' ),
			'after'  => '</div>',
		) );
	?>
	
	<div class="container">
		<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( (comments_open() || get_comments_number()) && !get_theme_mod('page_hide_comment') ) :
				comments_template();
			endif;
		?>
	</div>
