<?php
/**
 * @package rst
 */
?>

<div <?php post_class() ?> id="post-<?php the_ID(); ?> ">
	
	<!-- Main page -->
	<div class="rst-main-page" id="rst-portfoliosingle-page">
		<!-- Container -->
		<div class="container">
			
			<?php 
				the_content();
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( '<span>Pages:</span>', 'rst' ),
					'after'  => '</div>',
				) );
			?>
		</div>
		<!-- Container -->
		
		<div class="rst-portfoliosingle-nav">
			<div class="container">
				
				<?php echo previous_post_link('%link','PREV'); ?>

				<i class="fa fa-bars"></i>
				
				<?php echo next_post_link('%link','NEXT'); ?>
				
			</div>
		</div>
		
	</div>
	<!-- Main page -->
	
</div>
