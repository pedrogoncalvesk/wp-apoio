<?php
/**
 * rst functions and definitions
 *
 * @package rst
 */

 
 /**
 * Set the content width based on the theme's design and stylesheet.
 */
global $content_width;
if ( ! isset( $content_width ) ) {
 $content_width = 1170; /* pixels */
}

if ( ! function_exists( 'rst_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rst_setup() {
	
	if( !defined( 'LANGUAGE_ZONE' ) )
		define('LANGUAGE_ZONE', 'rst_multiup');
	
	
	
	/**
	 * rsLib.
	 */
	include(get_template_directory().'/rslib/rslib.php');
	
	include(get_template_directory().'/inc/custom-featured-image.php');
	include(get_template_directory().'/inc/ajax_action.php');
	
	//Rating
	include(get_template_directory() . '/inc/like/like.php');
	
	//Coming soon.php
	include(get_template_directory() . '/inc/coming-soon/coming-soon.php');
	
	// Import
	require get_template_directory() . '/rslib/import/rst-import-data-demo.php';
	
	// BFI
	get_template_part('extras/BFI_Thumb');
	
	/**
	 * Widget.
	 */
	include(get_template_directory().'/inc/widgets/widget-social-network.php');
	include(get_template_directory().'/inc/widgets/widget-recent-post.php');
	include(get_template_directory().'/inc/widgets/widget-contact-info.php');
	include(get_template_directory().'/inc/widgets/widget-contact-hour.php');
	include(get_template_directory().'/inc/widgets/widget-slider.php');
	
	/**
	 * Plugins.
	 */
	require get_template_directory() . '/inc/plugins/setup-plugins.php';
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'header_menu' => __( 'Header menu', 'rst' ),
		'footer_menu' => __( 'Footer Menu', 'rst' ),
	) );
	
	function rst_init() {
		
		/**
		 * Custom Field.
		 */
		include(get_template_directory().'/inc/custom-fields/page-template.php');
		include(get_template_directory().'/inc/custom-fields/post-formats.php');
		include(get_template_directory().'/inc/custom-fields/user.php');
		
		/**
		 * Customizer.
		 */
		include(get_template_directory().'/inc/add-customizer.php');
		
	}
	add_action('init','rst_init', 6);
	
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on rst, use a find and replace
	 * to change 'rst' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'rst', get_template_directory() . '/languages' );
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'image', 'video', 'audio', 'gallery'
	) );
	
	/**
	 * Register widget area.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
	 */
	function rst_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Sidebar', 'rst' ),
			'id'            => 'sidebar',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer column 1', 'rst' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer column 2', 'rst' ),
			'id'            => 'sidebar-2',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer column 3', 'rst' ),
			'id'            => 'sidebar-3',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer column 4', 'rst' ),
			'id'            => 'sidebar-4',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Subscriber', 'rst' ),
			'id'            => 'sidebar-5',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title"><span>',
			'after_title'   => '</span></h3>',
		) );
	}
	add_action( 'widgets_init', 'rst_widgets_init' );
	
	/**
	 * Enqueue scripts and styles.
	 */
	function rst_scripts() {
		
		wp_enqueue_style( 'css-bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
		wp_enqueue_style( 'css-bootstrap-jasny', get_template_directory_uri() . '/css/jasny-bootstrap.min.css' );
		wp_enqueue_style( 'css-font-awesome', get_template_directory_uri() . '/fonts/font-awesome-4.2.0/css/font-awesome.css' );
		wp_enqueue_style( 'css-effect', get_template_directory_uri() . '/css/effect2.css' );
		wp_enqueue_style( 'css-animate', get_template_directory_uri() . '/css/animate.css' );
		wp_enqueue_style( 'css-fontstrokehelper', get_template_directory_uri() . '/fonts/fontstroke/pe-icon-7-stroke/css/helper.css' );
		wp_enqueue_style( 'css-fontstroke', get_template_directory_uri() . '/fonts/fontstroke/pe-icon-7-stroke/css/pe-icon-7-stroke.css' );
		wp_enqueue_style( 'css-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css?v=2.1.5' );
		// wp_enqueue_style( 'css-customscrollbar', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.css' );
		wp_enqueue_style( 'css-owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css' );
		wp_enqueue_style( 'css-owltheme', get_template_directory_uri() . '/css/owl.theme.css' );
		wp_enqueue_style( 'css-owltransition', get_template_directory_uri() . '/css/owl.transitions.css' );
		wp_enqueue_style( 'css-rs', get_template_directory_uri() . '/css/rs-wp-v1.2.css' );
		wp_enqueue_style( 'css-main', get_template_directory_uri() . '/css/main.css' );
		wp_enqueue_style( 'css-responsive', get_template_directory_uri() . '/css/responsive.css' );
		
		wp_enqueue_style( 'rst-style', get_stylesheet_uri() );

		wp_enqueue_script( 'jquery' );
		
		wp_enqueue_script( 'js-blocks', get_template_directory_uri() . '/js/rst-blocks.js', array('jquery'), '', true );
		
		wp_enqueue_script( 'js-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '', true );
		// wp_enqueue_script( 'js-skrollr', get_template_directory_uri() . '/js/skrollr.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-counterup', get_template_directory_uri() . '/js/jquery.counterup.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-appear', get_template_directory_uri() . '/js/appear.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-isotope', get_template_directory_uri() . '/js/isotope.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.js?v=2.1.5', array('jquery'), '', true );
		wp_enqueue_script( 'js-fancybox-media', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6', array('jquery'), '', true );
		wp_enqueue_script( 'js-owlcarousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'), '', true );
		// wp_enqueue_script( 'js-customscrollbar', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.concat.min.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-bootstrapvalidation', get_template_directory_uri() . '/js/jqBootstrapValidation.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-backstretch', get_template_directory_uri() . '/js/jquery.backstretch.js', array('jquery'), '', true );
		wp_enqueue_script( 'js-countdown', get_template_directory_uri() . '/js/jquery.countdown.js', array('jquery'), '', true );
		
		if( get_theme_mod('rst_check_coming_soon') && !is_user_logged_in() && ! is_admin() ) {
			wp_enqueue_script( 'js-comming', get_template_directory_uri() . '/js/comming-soon.js', array('jquery'), '', true );
		}
		
		wp_enqueue_script( 'js-google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCgGyzOzpWh_mTpdx-UPt92W6GI8hE7P3M', array('jquery'), '', true ); 
		
		wp_enqueue_script( 'js-maplabel', get_template_directory_uri() . '/js/markerwithlabel.js', array('jquery'), '', true );
		
		
		wp_enqueue_script( 'js-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );
		

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'rst_scripts' );
	
	function rst_fonts() {
		wp_register_style('googleFonts-OpenSans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
		wp_enqueue_style( 'googleFonts-OpenSans');
		wp_register_style('googleFonts-Raleway', 'http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900');
		wp_enqueue_style( 'googleFonts-Raleway');
	}
	
	add_action('wp_print_styles', 'rst_fonts');
	
	
	function rst_backend_enqueue() {
		wp_enqueue_style( 'my_custom_script', esc_url( get_template_directory_uri() ) .'/inc/css/style-backend.css' );
		wp_enqueue_style( 'css-fontstrokehelper', get_template_directory_uri() . '/fonts/fontstroke/pe-icon-7-stroke/css/helper.css' );
		wp_enqueue_style( 'css-fontstroke', get_template_directory_uri() . '/fonts/fontstroke/pe-icon-7-stroke/css/pe-icon-7-stroke.css' ); 
	}
	add_action( 'admin_enqueue_scripts', 'rst_backend_enqueue', 10000 );
	
	
	function rst_after_customizer_css() {
		if( ! get_theme_mod('bg_banner') ) return;
    ?>
		<style type="text/css" id="customize-css">
		<?php
			$bg_banner = unserialize(get_theme_mod('bg_banner'));
			if( $bg_banner['background-color'] ) {
				echo '.rst-blog-banner:after { background-color: '. $bg_banner['background-color'] .' }';
			}
		?>
		</style>
		<?php
	}
	add_action( 'wp_head', 'rst_after_customizer_css' );
	
	/**
	 * Custom template tags for this theme.
	 */
	require get_template_directory() . '/inc/template-tags.php';

	/**
	 * Custom functions that act independently of the theme templates.
	 */
	require get_template_directory() . '/inc/extras.php';


	/**
	 * Load Jetpack compatibility file.
	 */
	require get_template_directory() . '/inc/jetpack.php';

	/* Translate */
	function rst_the_translate($text,$options_text) {
		$options_text = get_theme_mod($options_text);
		if($options_text) $text = $options_text;
		echo sanitize_text_field($text);
	}
	
	function rst_get_translate($text,$options_text) {
		$options_text = get_theme_mod($options_text);
		if($options_text) $text = $options_text;
		return sanitize_text_field($text);
	}
	
	function rst_get_my_widgets() {
		$sidebar_options = array(
			'0' => 'Select Sidebar'
		);
		foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
			$sidebar_options[$sidebar['id']] = $sidebar['name'];
		}
		return $sidebar_options;
	}

	add_action('init','rst_get_my_widgets');
	
	
	function rst_get_attachment_image_src( $attributes_id, $size ){
		$attributes = wp_get_attachment_image_src( $attributes_id, $size );
		return $attributes[0];
	}
	
	/*
	 * Function Count View
	 */
	function rst_set_post_views($postID) {
		$count_key = 'rst_post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '1');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}
	function rst_track_post_views ($post_id) {
		if ( !is_single() ) return;
		if ( empty ( $post_id) ) {
			global $post;
			$post_id = $post->ID;    
		}
		rst_set_post_views($post_id);
	}

	//To keep the count accurate, lets get rid of prefetching
	add_action( 'wp_head', 'rst_track_post_views');

	function rst_get_post_views($postID){
		$count_key = 'rst_post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count=='' || $count <= 1 ){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '1');
			return "1 View";
		}
		return $count.' Views';
	}
	
	
	function get_excerpt_by_id($post, $length = 0, $tags = '<a><em><strong>', $extra = '...') {
		if(is_int($post)) {
			$post = get_post($post);
		} elseif(!is_object($post)) {
			return false;
		}
		if( is_object($post) ) setup_postdata($post);
		
		if($length == 0) return apply_filters('the_content', $post->post_content);
		
		// $the_excerpt = apply_filters( 'get_the_excerpt', $post->post_excerpt );
		$the_excerpt = ( empty($post->post_excerpt) ) ? $post->post_content : $post->post_excerpt;
		
		$the_excerpt = strip_shortcodes(strip_tags($the_excerpt), $tags);
		
		$the_excerpt = preg_split('/\b/', $the_excerpt, $length * 2+1);
		$excerpt_waste = array_pop($the_excerpt);
		$the_excerpt = implode($the_excerpt);
		$the_excerpt = trim($the_excerpt);
		$the_excerpt = str_replace("\n","",$the_excerpt);
		$the_excerpt = str_replace("\r","",$the_excerpt);
		
		if( $the_excerpt != '' )
			$the_excerpt .= $extra;
		wp_reset_postdata();
		
		return apply_filters('the_content', $the_excerpt);
	}
	
	function rst_get_template_part( $slug, $name="" ) {
		ob_start();
		get_template_part( $slug, $name );
		return ob_get_clean();
	}
	
	/**
	 * Paging Nav.
	 */
	function rst_paging_nav(){
		global $wp_query;
		$tf_big = 999999999;
		$my_page = max( get_query_var('paged'), get_query_var('page') );
		$tf_paginate_links = paginate_links( array(
			'base' => str_replace( $tf_big, '%#%', esc_url( get_pagenum_link( $tf_big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, $my_page ),
			'total' => $wp_query->max_num_pages,
			'prev_text'    => __('&#171;','yup'),
			'next_text'    => __('&#187;','yup'),
			'type'			=> 'array'
		 ));
		
		if( is_array( $tf_paginate_links ) ) {
			$paged = ( $my_page == 0 ) ? 1 : $my_page;
			echo '<ul class="wp-pagenavi">';
			foreach ( $tf_paginate_links as $page ) {
				echo force_balance_tags($page);
			}
			echo '</ul>';
		}
	}
	
	/**
	 * Paging Nav Vc.
	 */
	function workaroundpaginationampersandbug($link) {
		return str_replace('#038;', '&', $link);
	}
	add_filter('paginate_links', 'workaroundpaginationampersandbug');
	
	function rst_paging_nav_vc($wp_query,$ub_agrs,$rstkey=0){
		$html = '';
		global $paged;
		if( $ub_agrs['is_show_pavi'] == 'number' ) {
			$tf_big = 999999999;
			$my_page = max( get_query_var('paged'), get_query_var('page') );
			$tf_paginate_links = paginate_links( array(
				'base' => str_replace( $tf_big, '%#%', esc_url( get_pagenum_link( $tf_big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, $my_page ),
				'total' => $wp_query->max_num_pages,
				'prev_text'    => __('&#171;','yup'),
				'next_text'    => __('&#187;','yup'),
				'type'			=> 'array'
			 ));
			
			if( is_array( $tf_paginate_links ) ) {
				$paged = ( $my_page == 0 ) ? 1 : $my_page;
				$html .= '<ul class="wp-pagenavi">';
				foreach ( $tf_paginate_links as $page ) {
					$html .= $page;
				}
				$html .= '</ul>';
			}
		}
		if( $ub_agrs['is_show_pavi'] == 'next_prev' ) {
			ob_start();
			$args = ub_query_shortcode($ub_agrs);
			if ( $wp_query->max_num_pages > 1 ) :  ?>
			
			<nav class="row navigation" role="navigation">
				<div class="col-sm-6 nav-previous"><?php previous_posts_link( '<i class="fa fa-angle-double-left"></i> Prev Page', $wp_query ->max_num_pages ); ?></div>
				<div class="col-sm-6 nav-next"><?php next_posts_link( 'Next Page <i class="fa fa-angle-double-right"></i>', $wp_query ->max_num_pages ); ?></div>
			</nav>
			
			<?php endif;
			$html .= ob_get_contents();
			ob_end_clean();
		}
		if( $ub_agrs['is_show_pavi'] == 'load_more' ) {
			if( $wp_query->max_num_pages > 1 ) {
				$html .= '<a href="javascript:;" data-paged="2" data-disable="0"  data-max-paged="'. $wp_query->max_num_pages .'" data-key="rst_'. $rstkey .'" class="rst_ajax_load_more"><i class="fa fa-spinner fa-pulse"></i><span>Load more</span></a>';
			}
		}
		
		if( $ub_agrs['is_show_pavi'] == 'load_auto' ) {
			if( $wp_query->max_num_pages > 1 ) {
				$html .= '<a href="javascript:;" data-paged="2" data-disable="0" data-max-paged="'. $wp_query->max_num_pages .'" data-key="rst_'. $rstkey .'" class="rst_ajax_load_more rst_auto_load"><i class="fa fa-spinner fa-pulse"></i></a>';
			}
		}
		
		return $html;
	}
	
	function rst_get_the_archive_title($before="",$after="") {
		if ( is_category() ) {
			$title = rst_get_translate('Category','translation_category') .': '. single_cat_title( '', false );
		} elseif ( is_tag() ) {
			$title = rst_get_translate('Browsing Tag','browsing_tag') .': '. single_tag_title( '', false );
		} elseif ( is_author() ) {
			$title = rst_get_translate('Author','translation_author') .': '. get_query_var('author_name');
		} elseif ( is_year() ) {
			$title = rst_get_translate('Yearly Archives','yearly_archives').': '. get_the_date( 'Y');
		} elseif ( is_month() ) {
			$title = rst_get_translate('Monthly Archives','monthly_archives').': '. get_the_date( 'F Y');
		} elseif ( is_day() ) {
			$title = rst_get_translate('Daily Archives','daily_archives').': '. get_the_date( 'F j, Y');
		} elseif ( is_tax( 'post_format' ) ) {
			if ( is_tax( 'post_format', 'post-format-aside' ) ) {
				$title = 'Asides';
			} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
				$title = 'Galleries';
			} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
				$title = 'Images';
			} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
				$title = 'Videos';
			} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
				$title = 'Quotes';
			} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
				$title = 'Links';
			} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
				$title = 'Statuses';
			} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
				$title = 'Audio';
			} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
				$title = 'Chats';
			}
		} elseif ( is_post_type_archive() ) {
			$title = rst_get_translate('Archives','translation_archives') .': ' . post_type_archive_title( '', false );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = $tax->labels->singular_name. ': '. single_term_title( '', false );
		} else {
			$title = rst_get_translate('Archives','translation_archives');
		}
	 
		/**
		 * Filter the archive title.
		 *
		 * @since 4.1.0
		 *
		 * @param string $title Archive title to be displayed.
		 */
		return apply_filters( 'get_the_archive_title', $before.$title.$after );
	}
	
	add_filter( 'request', 'alter_the_query' );
	function alter_the_query( $request ) {
		if( isset($request['cat']) && !(is_admin()) ){
			$request['posts_per_page'] = 1;
		}
		return $request;
	}
	
	/**
	 * Custom Comment.
	 */
	include(get_template_directory().'/inc/comments.php');
	
}
endif; // rst_setup
add_action( 'after_setup_theme', 'rst_setup',999 );
