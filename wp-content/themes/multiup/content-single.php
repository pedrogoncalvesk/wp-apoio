<?php
/**
 * @package rst
 */
?>

<div <?php post_class() ?> id="post-<?php the_ID(); ?> ">
			
			
	<div class="container rst-blog-page-container">
		
		<div class="row">
		
			<div class="rst-blog-page <?php if( ! get_theme_mod('single_hide_sidebar') ) { echo 'col-sm-9';} ?>">
		
				<div class="rst-blog-info">
					<?php if( !get_theme_mod('singe_hide_date') ) { ?>
					<time datetime="<?php echo mysql2date('Y-m-d', $post->post_date) ?>" itemprop="dateCreated" ><?php echo mysql2date('d F Y', $post->post_date) ?></time>
					<?php } ?>
					<ul>
						<?php if( ! get_theme_mod('disable_likepost') ) { ?>
						<li><?php echo rst_render_like($post->ID) ?></li>
						<?php } ?>
						<?php if( ! get_theme_mod('singe_hide_comment_count') ) { ?>
						<li><a href="<?php the_permalink(); ?>#comments"><i class="fa fa-comment-o"></i> <span itemprop="interactionCount"><?php comments_number('0', '1', '%');?></span></a></li>
						<?php } ?>
						<?php if( ! get_theme_mod('singe_hide_author') ) { ?>
						<li>
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
								<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
								<span><?php the_author() ?></span>
							</a>
						</li>
						<?php } ?>
					</ul>
					<div class="clear"></div>
				</div>
				
				<div class="entry">
				
					<?php 
						if( ! get_theme_mod('singe_hide_thumbnail') ) :
							get_template_part( 'thumbnail', get_post_format() );
						endif;
					?>
					
					<?php the_content(); ?>

				</div>
				
				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( (comments_open() || get_comments_number()) && ! get_theme_mod('singe_hide_comment') ) :
						comments_template();
					endif;
				?>
				
			</div>
			
			<?php
				if( ! get_theme_mod('single_hide_sidebar') ) {
			?>
			<div class="rst-page-has-sidebar col-sm-3">
				<?php
					if ( is_active_sidebar( 'sidebar' ) ) {
						dynamic_sidebar( 'sidebar' );
					}
				?>
			</div>
			<?php } ?>
			
		</div>
		
	</div>
	
</div>
