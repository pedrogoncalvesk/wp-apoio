<?php
	global $rst_blog;
	$count_image = $rst_blog['number_gallery'] ? $rst_blog['number_gallery'] : 7;
	$height_gallery = $rst_blog['height_gallery'] ? $rst_blog['height_gallery'] : 150;
	
	if( is_single() ) {
		$count_image = 999;
		$height_gallery = rs::getField('rst_single_height_gallery',get_the_ID()) ? rs::getField('rst_single_height_gallery',get_the_ID()) : 150;
	}
?>
<div class="rst-thumbnail">
	<?php if( rs::getField('rst_gallery') ) { ?>
	
	<div class="owl-carousel owl-blogsingle" data-height="<?php echo absint( $height_gallery ) ?>">
		<?php 
			foreach( rs::getField('rst_gallery') as $key=>$gallery ) {
				if( $key+1 > $count_image ) break;
		?>
		<img src="<?php echo esc_url( rst_get_attachment_image_src( $gallery, 'full' )) ?>" alt="" />
		<?php } ?>
	</div>
	
	
	<?php } else {
		the_post_thumbnail();
	} ?>
</div>