
jQuery(document).ready(function($){
	//Comming
	$('html').addClass('template-coming');
	// Countdown
	var date_comming = jQuery('.rst-coming-countdown').attr('data-date');
	if( date_comming != undefined ) {
	jQuery('.rst-coming-countdown').countdown(date_comming, function(event) {
		jQuery(this).html(event.strftime('<div class="rst-times">'
		+ '<div class="rst-time"><span>%D</span><p>DAYS</p></div>'
		+ '<div class="rst-time"><span>%H</span><p>HOURS</p></div>'
		+ '<div class="rst-time"><span>%M</span><p>MINUTES</p></div>'
		+ '<div class="rst-time"><span>%S</span><p>SECONDS</p></div>'
		+ '<div class="clear"></div>'
		+ '</div>'));
	});
	}
});