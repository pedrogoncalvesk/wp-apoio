<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rst
 */

 get_header(); ?>
 
		<!-- Header banner -->
		<div class="rst-header-banner">
			<div class="container">
				<div class="rst-banner-content">
					<div class="rst-banner-title">
						<h1><?php echo get_theme_mod('header_title') ? get_theme_mod('header_title') : 'Welcome to Multiup' ?></h1>
					</div>
					<div class="rst-scroll-down">
						<a href="#"><i class="fa fa-chevron-down"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- Header banner -->
		
	</div>
</header>
<!-- Header -->

<!-- Main page -->
<div class="rst-main-page archive-page wpb_row" id="rst-blog-page">

	<?php
		global $rst_blog;
		$rst_blog['excerpt_length'] = 30;
		$rst_blog['disable_animate'] = false;
		
		$post_per_page = 10;
		$args = array(
			'posts_per_page' => $post_per_page,
			'paged' 			=> max( get_query_var( 'paged' ), get_query_var( 'page' ))
		);
		$args = array_merge( $wp_query->query_vars, $args );
		$the_query = new WP_Query( $args );
		$wp_query = $the_query;
	?>
	
	<div class="container rst-blog-page-container">

		<div class="row">
		
			<div class="rst-blog-page">
			
				<?php while ($the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; ?>
				
				<!-- Page navigation -->
				<div class="rst-page">
					<?php rst_the_posts_navigation(); ?>
				</div>
				<!-- Page navigation -->
				
			</div>
	
		</div>
		
</div>
<!-- Blog container -->
	
</div>
<!-- Main page -->
		
<?php get_footer(); ?>		