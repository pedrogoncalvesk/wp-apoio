<?php
class rs_import extends WP_Import
{
    function import_customize( $file ) {
		// File exists?
		if ( ! file_exists( $file ) ) {
			wp_die(
				__( 'Import file could not be found. Please try again.', 'widget-importer-exporter' ),
				'',
				array( 'back_link' => true )
			);
		}
		
		$data = file_get_contents( $file );
		$data = json_decode( $data,true );
		if( $data ) {
			foreach( $data as $name=>$customize ) {
				set_theme_mod( $name, $customize );
			}
		}
    }
}
