<?php
	require get_template_directory() . '/rslib/import/rs-import.php';
	
	add_action('admin_menu', 'rst_register_menu_page');

	function rst_register_menu_page() {
		 add_theme_page( 'Import Demo Data', 'Import Data', 'edit_theme_options', 'rst-import-data', 'rst_custom_menu_page_import' );
	}

	function rst_custom_menu_page_import(){
	?>
		<div id="poststuff" class="rst-import">
			<div class="postbox  hide-if-js" id="postexcerpt" style="display: block;">
				<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle"><span>Import Demo Data</span></h3>
				<div class="inside">
					<p>
					<a href="javascript:;" class="button button-primary rst-import-action">Import Demo Data</a>
					<br><br>Warning: You must import the sample data file before customizing your theme.
					If you customize your theme, and later import a sample data file, all current contents entered in your site will be overwritten to the default settings of the file you are uploading! Please proceed with the utmost care, after exporting all current data!
					Note: If you get errors, please be sure that your server configured Memory Limit &gt;=64MB and Execution Time &gt;=60.</p>
				</div>
				<div class="inside">
					<p class="import-message">
						<span class="import_message"></span>
						<br style="clear:both" />
					</p>
				</div>
			</div>
		</div>
	<?php
	}
?>