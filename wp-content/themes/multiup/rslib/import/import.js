jQuery(function($){
	jQuery('.rst-import-action').click(function(){
		$import_true = confirm('Are you sure to import dummy content ? It will overwrite the existing data');
		if($import_true == false) return;
		
		var data = {'action': 'rs_import' };
		
		jQuery('.rst-import .import_message').html('<i class="spinner" style="float: none; margin: 0px; vertical-align: middle;visibility: visible;"></i> Data is being imported please be patient, while the awesomeness is being created :)');

		
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('.rst-import .import_message').html(response);
		});
		return false;
	});
})