<?php
if( !function_exists( 'tf_load_custom_code_style' ) ){
	function rs_import_callback() 
	{
		global $wpdb;
		
		if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

		// Load Importer API
		require_once ABSPATH . 'wp-admin/includes/import.php';
		
		require get_template_directory() . "/rslib/import/import-widget.php";

		if ( ! class_exists( 'WP_Importer' ) ) {
			$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
			if ( file_exists( $class_wp_importer ) )
			{
				require $class_wp_importer;
			}
			
		}

		if ( ! class_exists( 'WP_Import' ) ) {
			$class_wp_importer = get_template_directory() ."/rslib/import/wordpress-importer.php";
			if ( file_exists( $class_wp_importer ) )
				require $class_wp_importer;
		}


		if ( class_exists( 'WP_Import' ) ) 
		{
			// Registe class rs_import
			require get_template_directory() . "/rslib/import/rs-import-customize.php";
			
			// Import Post/Page
			$import_filepath = get_template_directory() ."/rslib/import/data/multiup.wordpress.2015-08-28.xml" ; // Get the xml file from directory 
			
			$wp_import = new rs_import();
			$wp_import->fetch_attachments = true;
			$wp_import->import($import_filepath);
			
			// Import Customize
			// var_dump( json_encode(get_theme_mods()) );exit;
			$wp_import->import_customize( get_template_directory(). "/rslib/import/data/lazathemes-customize.txt" );
			remove_theme_mod( 'nav_menu_locations' );
			
			// Import Widget
			$file_widget = get_template_directory() ."/rslib/import/data/lazathemes-demo-widgets.wie" ;
			wie_process_import_file( $file_widget );
			
			// Import Page Home
			update_option('show_on_front','page');
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'page',
			);
			$posts_array = get_posts( $args );
			foreach($posts_array as $item) {
				if( $item->post_title == 'Home 2' )
					update_option('page_on_front',$item->ID);
			}
			
		}
		die(); // this is required to return a proper result
	}
}
function rst_admin_import_scripts() {
    wp_register_script( 'rs-import', get_template_directory_uri() . '/rslib/import/import.js', false, '1.0.0' );
    wp_enqueue_script( 'rs-import' );
}
add_action( 'admin_enqueue_scripts', 'rst_admin_import_scripts' );
add_action( 'wp_ajax_rs_import', 'rs_import_callback' );
