<?php
class WP_Customize_Term_List_Control extends WP_Customize_Control {
    public $type = 'term-list';
 
    public function render_content() {
       
		$name = '_customize-term-list-' . $this->id;

		if ( ! empty( $this->label ) ) : ?>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
		<?php endif;
		if ( ! empty( $this->description ) ) : ?>
			<span class="description customize-control-description"><?php echo force_balance_tags($this->description) ; ?></span>
		<?php endif;
		
		$taxonomy = isset($this->taxonomy) ? $this->taxonomy : 'category';
		
		$args = array(
			'orderby'           => 'name', 
			'order'             => 'ASC',
			'hide_empty'        => true, 
			'exclude'           => array(), 
			'exclude_tree'      => array(), 
			'include'           => array(),
			'number'            => '', 
			'fields'            => 'all', 
			'slug'              => '',
			'parent'            => '',
			'hierarchical'      => true, 
			'child_of'          => 0,
			'childless'         => false,
			'get'               => '', 
			'name__like'        => '',
			'description__like' => '',
			'pad_counts'        => false, 
			'offset'            => '', 
			'search'            => '', 
			'cache_domain'      => 'core'
		); 
		
		$query = isset($this->query) ? array_merge($args, $this->query) : $args;
		
		
		$elements = get_terms( $taxonomy, $query );
		?>
			<select <?php $this->link(); ?>>
				<?php foreach( $elements as $item ) { ?>
				<option <?php echo selected( $this->value(), $item->slug, false ) ?> value="<?php echo $item->slug ?>"><?php echo $item->name ?></option>
				<?php } ?>
			</select>
		<?php
    }
	
}