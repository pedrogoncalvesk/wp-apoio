<nav class="rst-social">
	<?php if( get_theme_mod('social_facebook') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_facebook')) ?>"><i class="fa fa-facebook"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_google') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_google')) ?>"><i class="fa fa-google-plus"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_twitter') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_twitter')) ?>"><i class="fa fa-twitter"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_tumblr') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_tumblr')) ?>"><i class="fa fa-tumblr"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_instagram') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_instagram')) ?>"><i class="fa fa-instagram"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_youtube') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_youtube')) ?>"><i class="fa fa-youtube"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_linkedin') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_linkedin')) ?>"><i class="fa fa-linkedin"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_flickr') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_flickr')) ?>"><i class="fa fa-flickr"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_vimeo') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_vimeo')) ?>"><i class="fa fa-vimeo-square"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_pinterest') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_pinterest')) ?>"><i class="fa fa-pinterest-p"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_dribbble') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_dribbble')) ?>"><i class="fa fa-dribbble"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_digg') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_digg')) ?>"><i class="fa fa-digg"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_skype') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_skype')) ?>"><i class="fa fa-skype"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_deviantart') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_deviantart')) ?>"><i class="fa fa-deviantart"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_yahoo') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_yahoo')) ?>"><i class="fa fa-yahoo"></i></a>
	<?php } ?>
	<?php if( get_theme_mod('social_reddit') ) { ?>
	<a target="_blank" href="<?php echo esc_url(get_theme_mod('social_reddit')) ?>"><i class="fa fa-reddit"></i></a>
	<?php } ?>
</nav>