<?php
if( ! function_exists( 'lz_vc_testimonial' ) ) {
	function lz_vc_testimonial( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'post'						=> '',
			'layout'					=> '',
			'color'						=> '',
			'size'						=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'			=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['post'] = isset($attr['post']) ? $attr['post'] : '';
		$attr['layout'] = isset($attr['layout']) ? $attr['layout'] : 1;
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '#212f44';
		$attr['size'] = isset($attr['size']) ? $attr['size'] : '22px';
		$attr['is_show_image'] = isset($attr['is_show_image']) ? $attr['is_show_image'] : false;
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		if( $attr['post'] ) {
			$post_testimonial = get_page_by_path( $attr['post'], OBJECT, 'testimonials' );
			
			if( $attr['layout'] == 1 ) $class[] = 'rst-testimonial';
			if( $attr['layout'] == 2 ) $class[] = 'rst-author';
			if( $attr['layout'] == 3 ) $class[] = 'rst-talkbox';
			
			$html .= '<div class="'. rst_class($class) . ' ">';
			
				$html .= '<p style="color:'. $attr['color'] .'"><span style="font-size: '. $attr['size'] .'">'. rs::getField('rst_testimonial_text',$post_testimonial->ID) .'</span></p>';
				
				if( $attr['layout'] == 2 && $attr['is_show_image'] ) {
					if( get_post_thumbnail_id($post_testimonial->ID) ) {
						$html .= '<div class="text-center"><img src="'.wp_get_attachment_url( get_post_thumbnail_id($post_testimonial->ID) ).'" alt="" /></div>';
					}
				}
				$html .= '<cite style="color:'. $attr['color'] .'">'. rs::getField('rst_testimonial_name',$post_testimonial->ID) .'</cite>';
		
			$html .= '</div>';
					
			if( $attr['layout'] != 2 && $attr['is_show_image'] ) {
				if( get_post_thumbnail_id($post_testimonial->ID) ) {
					$html .= '<div class="rst-whatpeolesay"><img src="'.wp_get_attachment_url( get_post_thumbnail_id($post_testimonial->ID) ).'" alt="" /></div>';
				}
			}
		
		}
		else {
			$html .= 'Please choose post.';
		}
		
		return $html;
	}
}
add_shortcode( 'lz_testimonial', 'lz_vc_testimonial' );


vc_map( array (
	'base' 			=> 'lz_testimonial',
	'name' 			=> __('LZ Testimonial', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'post',
			'type' 			=> 'postlist',
			'heading' 		=> __('Select Testimonial', 'mfn-opts'),
			'admin_label'	=> true,
			'query'			=> array(
				'post_type'	=> 'testimonials'
			)
		),
		array (
			'param_name' 	=> 'layout',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Style', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("Style 1", LANGUAGE_ZONE) => "1", 
							__("Style 2", LANGUAGE_ZONE) => "2",
							__("Style 3", LANGUAGE_ZONE) => "3",
			),
		),
		array (
			'param_name' 	=> 'color',
			'type' 			=> 'colorpicker',
			'heading' 		=> __('Text Color', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '#212f44'
		),
		array (
			'param_name' 	=> 'size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Font Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '22px'
		),
		array (
			'param_name' 	=> 'is_show_image',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Show Image', 'mfn-opts'),
			'admin_label'	=> true
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
