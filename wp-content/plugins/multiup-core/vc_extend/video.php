<?php
if( ! function_exists( 'lz_vc_video' ) ) {
	function lz_vc_video( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'image'					=> '',
			'link'					    => '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['image'] = isset($attr['image']) ? $attr['image'] : '';
		$attr['link'] = isset($attr['link']) ? $attr['link'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$img = wp_get_attachment_image_src($attr['image'], '');
        $imgSrc = $img[0];
		
		$link = $attr['link'];
		$signal = substr($link,-12,1);
		if ( $signal == '=' ) {
			$pieces = explode("=", $link);
			$link = 'https://www.youtube.com/embed/';
			$link .= $pieces[1];
		} else {
			$pieces = explode("/", $link);
			$link = 'https://player.vimeo.com/video/';
			$number = sizeof($pieces) - 1;
			$link .= $pieces[$number];
		}
		
		$html .= '<div class="rst-singlegroupvideo">';
			$html .= '<div class="container">';
				$html .= '<div class="rst-video">';
					$html .= '<img alt="" src="'. $imgSrc .'">';
					$html .= '<a class="fancybox-media control-video" href="'. $link .'"><i class="pe-7s-play"></i></a>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
				
		return $html;
	}
}
add_shortcode( 'lz_video', 'lz_vc_video' );


vc_map( array (
	'base' 			=> 'lz_video',
	'name' 			=> __('LZ Video', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'image',
			'type' 			=> 'attach_image',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Background', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'link',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Video Link', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'Only supports video from youtube and vimeo',          
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
