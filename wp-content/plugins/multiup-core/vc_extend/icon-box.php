<?php
if( ! function_exists( 'lz_vc_box_info' ) ) {
	function lz_vc_box_info( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'info'					        => '',
			'icon'							=> '',
			'icon_position'					=> '',
			'icon_size'						=> '',
			'icon_color'					=> '',
			'border_width'					=> '',
			'enable_animate'				=> '',
			'animate'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['info'] = isset($attr['info']) ? $attr['info'] : '';
		$attr['icon'] = isset($attr['icon']) ? $attr['icon'] : '';
		$attr['icon_position'] = isset($attr['icon_position']) ? $attr['icon_position'] : 'left';
		$attr['icon_size'] = isset($attr['icon_size']) ? $attr['icon_size'] : '38px';
		$attr['icon_color'] = isset($attr['icon_color']) ? $attr['icon_color'] : '#2ecc71';
		$attr['border_width'] = isset($attr['border_width']) ? $attr['border_width'] : '1px';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		if( $attr['icon_position'] != 'top' ) {
			
			if ($attr['icon_position']=='right') $class[] = 'rst-revert';
			
			$html .= '<div class="rst-boxinfo-style-1 '. rst_class($class) .'">';
				if( $attr['icon'] ) {
					$html .= '<span class="rst-icon"><i style="color:'. $attr['icon_color'] .';font-size:'. $attr['icon_size'] .'" class="'. $attr['icon'] . '"></i></span>';
				}
				if( $attr['title'] ) {
					$html .= '<span>'. $attr['title'] .'</span>';
				}
				if( $attr['info'] ) {
					$html .= '<p>'. $attr['info'] .'</p>';
				}
			$html .= '</div>';
		
		}
		else {
			$html .= '<div class="rst-box-info-2 '. rst_class($class) .' ">';
				if( $attr['icon'] ) {
					$html .= '<span class="rst-icon"><i style="color:'. $attr['icon_color'] .';font-size:'. $attr['icon_size'] .';border-width:'. $attr['border_width'] .'" class="'. $attr['icon'] . '"></i></span>';
				}
				if( $attr['title'] ) {
					$html .= '<span>'. $attr['title'] .'</span>';
				}
				if( $attr['info'] ) {
					$html .= '<p>'. $attr['info'] .'</p>';
				}
			$html .= '</div>';
		}
		return $html;
	}
}
add_shortcode( 'lz_box_info', 'lz_vc_box_info' );


vc_map( array (
	'base' 			=> 'lz_box_info',
	'name' 			=> __('LZ IconBox', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'info',
			'type' 			=> 'textarea',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'icon',
			'type' 			=> 'rst_icons',
			'heading' 		=> __('Icon', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
			"type" => "dropdown",
			"heading" => __("Icon Position", LANGUAGE_ZONE),
			"param_name" => "icon_position",
			"admin_label" => true,
			"value" => array(__("Left", LANGUAGE_ZONE) => 'left', __("Right", LANGUAGE_ZONE) => "right", __("Top", LANGUAGE_ZONE) => "top"),
		),
		array (
			'param_name' 	=> 'icon_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Icon Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '38px'
		),
		array (
			'param_name' 	=> 'icon_color',
			'type' 			=> 'colorpicker',
			'heading' 		=> __('Icon Color', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '#2ecc71'
		),
		array (
			'param_name' 	=> 'border_width',
			'type' 			=> 'textfield',
			'heading' 		=> __('Border Width', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '1px'
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
