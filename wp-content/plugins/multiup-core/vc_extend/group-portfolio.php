<?php
if( ! function_exists( 'lz_vc_group_portfolio' ) ) {
	function lz_vc_group_portfolio( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'style'								=> '',
			'portfolio_terms'					=> '',
			'column'							=> '',
			'number'							=> '',
			'is_show_pagenavi'					=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'					=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['style'] = isset($attr['style']) ? $attr['style'] : 1;
		$attr['portfolio_terms'] = isset($attr['portfolio_terms']) ? $attr['portfolio_terms'] : '';
		$attr['column'] = isset($attr['column']) ? $attr['column'] : '';
		$attr['number'] = isset($attr['number']) ? $attr['number'] : '';
		$attr['is_show_pagenavi'] = isset($attr['is_show_pagenavi']) ? $attr['is_show_pagenavi'] : false;
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		ob_start();
		
		if( $attr['style'] == 1 ) {
			echo '<div class="' . rst_class($class) . ' ">';
			
			$argv = array(
				'orderby'       =>  'term_order',
				'hide_empty'    => false
			);
			if( $attr['portfolio_terms'] ) {
				$argv['slug'] = explode(',',$attr['portfolio_terms']);
			}
			$listterm = get_terms('portfolio-categories', $argv);
			
			echo '<div class="rst-portfolio-cat">';
				echo '<ul>';
					echo '<li class="active"><a class="filter" data-filter="*" href="#">'. rst_get_translate('All','trans_all') .'</a></li>';
			
					foreach ( $listterm as $item ) {
						$slug = $item->slug;
						echo '<li><a class="filter" data-filter=".'.$slug.'" href="#">'.$slug.'</a></li>';
					}

				echo '</ul>';
			echo '</div>';
			
			
			echo '<div class="rst-portfolio-list isotope">';
				
				$numberpost = absint($attr['number']);
				if ( $numberpost == 0 )
				$numberpost = 6;
				
				$sidebar_args = array(
					'post_status' => 'publish',
					'post_type' => 'portfolio', //CHANGE IT
					'posts_per_page' => $numberpost,
					'orderby' => 'date',
					'order' => 'ASC',
					'paged' => max( get_query_var( 'paged' ), get_query_var( 'page' )) 
				);
				if( $attr['portfolio_terms'] ) {
					$sidebar_args['tax_query'] = array(
						array(
							'taxonomy' => 'portfolio-categories',
							'field' => 'slug',
							'terms' => explode(',',$attr['portfolio_terms'])
						)
					);
				}
				$number = 1;
				global $wp_query;
				$old_query = $wp_query;
				$testimonials = new WP_Query($sidebar_args);
				
				$wp_query = $testimonials;
				if( $testimonials->have_posts() ){
					while ($testimonials->have_posts()) : $testimonials->the_post();
						$post_id = get_the_ID();
						$terms = get_the_terms( get_the_ID() , 'portfolio-categories' );
						
						$value = '';
						
						foreach( $terms as $term ) {
							$value .= $term->slug;
							$value .= ' ';
						}
						
						$number = str_pad($number, 2, '0', STR_PAD_LEFT);  
						
						
						echo '<div class="rst-portfolio '.$value.'">';
							echo '<div class="rst-img">';
								if( wp_get_attachment_url( get_post_thumbnail_id() ) ) {
								echo '<img alt="" src="'.wp_get_attachment_url( get_post_thumbnail_id() ).'">';
								}
								echo '<span>'.$number.'</span>';
							echo '</div>';
							echo '<div class="rst-info">';
								echo '<p class="rst-info-title"><a href="'.get_permalink().'">'.get_the_title().'</a></p>';
								echo get_excerpt_by_id($post_id,30);
								echo '<a class="btn" href="'.get_permalink($post_id).'">View project</a>';
							echo '</div>';
							echo '<div class="clear"></div>';
						echo '</div>';
						
						
						$number += 1;
						
					endwhile;
				}
					echo '<div class="clear"></div>';
				echo '</div>';
				
				if( $attr['is_show_pagenavi'] ) {
				echo '<div class="rst-page">';
					rst_the_posts_navigation();
				echo '</div>';
				}
				
			echo '</div>';
		}
		else {
			$column = '';
			
			if ( $attr['column'] == '4') {
				$column = 'rst-col4';
			}
			
			echo '<div class="rst-work ' . rst_class($class) . ' ' . $column . ' ">';
				
				
				$argv = array(
					'orderby'       =>  'term_order',
					'hide_empty'    => false
				);
				if( $attr['portfolio_terms'] ) {
					$argv['slug'] = explode(',',$attr['portfolio_terms']);
				}
				$listterm = get_terms('portfolio-categories', $argv);
				
				echo '<div class="rst-portfolio-cat">';
					echo '<ul>';
						echo '<li class="active"><a class="filter" data-filter="*" href="#">All</a></li>';
				
						foreach ( $listterm as $item ) {
							$slug = $item->slug;
							echo '<li><a class="filter" data-filter=".'.$slug.'" href="#">'.$slug.'</a></li>';
						}

					echo '</ul>';
				echo '</div>';
				
				
				echo '<div class="rst-portfolio-list isotope">';
					
					$numberpost = absint($attr['number']);
					
					$sidebar_args = array(
						'post_status' => 'publish',
						'post_type' => 'portfolio', //CHANGE IT
						'posts_per_page' => $numberpost,
						'paged' => max( get_query_var( 'paged' ), get_query_var( 'page' )),
						'orderby' => 'date',
						'order' => 'ASC'
					);
					if( $attr['portfolio_terms'] ) {
						$sidebar_args['tax_query'] = array(
							array(
								'taxonomy' => 'portfolio-categories',
								'field' => 'slug',
								'terms' => explode(',',$attr['portfolio_terms'])
							)
						);
					}
					$number = 1;
					
					global $wp_query;
					$old_query = $wp_query;
					$testimonials = new WP_Query($sidebar_args);
					
					$wp_query = $testimonials;
					
					if( $testimonials->have_posts() ){
						while ($testimonials->have_posts()) : $testimonials->the_post();
							
							$terms = get_the_terms( get_the_ID() , 'portfolio-categories' );
							
							$value = '';
							
							foreach( $terms as $term ) {
								$value .= $term->slug;
								$value .= ' ';
							}
							
							$number = str_pad($number, 2, '0', STR_PAD_LEFT);  
							
							echo '<div class="rst-portfolio '.$value.'">';
							
								echo '<div class="rst-img">';
									if( wp_get_attachment_url( get_post_thumbnail_id() ) ) {
									echo '<a href="'.get_post_permalink().'"><img alt="" src="'. wp_get_attachment_url( get_post_thumbnail_id() ) .'"></a>';
									}
									echo '<span>'.$number.'</span>';
									echo '<p>'.get_the_title().'</p>';
								echo '</div>';
								echo '<div class="clear"></div>';
							echo '</div>';
							
							$number += 1;
							
						endwhile;
					}

					echo '<div class="clear"></div>';
				echo '</div>';
				
				if( $attr['is_show_pagenavi'] ) {
				echo '<div class="rst-page">';
					rst_the_posts_navigation();
				echo '</div>';
				}
				
			echo '</div>';
		}
		
		wp_reset_postdata();
		$html = ob_get_contents();
		ob_end_clean();
		$wp_query = $old_query;
		return $html;
	}
}
add_shortcode( 'lz_group_portfolio', 'lz_vc_group_portfolio' );

vc_map( array (
	'base' 			=> 'lz_group_portfolio',
	'name' 			=> __('LZ Portfolio', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array(
			"type" => "dropdown",
			"heading" => __("Style", LANGUAGE_ZONE),
			"param_name" => "style",
			"admin_label" => true,
			"value" => array(__("Row", LANGUAGE_ZONE) => '1', __("Columns", LANGUAGE_ZONE) => "2"),
		),
		array(
			"type" => "terms",
			"heading" => __("Portfolio Cagetories", LANGUAGE_ZONE),
			"param_name" => "portfolio_terms",
			"admin_label" => true,
			'taxonomies'	=> 'portfolio-categories'
		),
		array(
			"type" => "dropdown",
			"heading" => __("Column", LANGUAGE_ZONE),
			"param_name" => "column",
			"admin_label" => true,
			"value" => array(__("3 Column", LANGUAGE_ZONE) => '3', __("4 Column", LANGUAGE_ZONE) => "4"),
			"dependency" => array(
				"element" => "style",
				"value" => array(
					"2"
				)
			)
		),
		array (
			'param_name' 	=> 'number',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number post', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'Enter the number of the portfolio you want to display.',                                                     
		),
		array (
			'param_name' 	=> 'is_show_pagenavi',
			'type' 			=> 'checkbox',
			'heading' 		=> __('Show PageNavi', 'mfn-opts'),
			'admin_label'	=> true                                                   
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
