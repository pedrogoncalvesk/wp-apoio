<?php
if( ! function_exists( 'lz_vc_progressbar' ) ) {
	function lz_vc_progressbar( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'percent'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['percent'] = isset($attr['percent']) ? $attr['percent'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$html .= '<div class="rst-skills-progess ' . $attr['extraclassname'] . ' ">';
			$html .= '<p>'. $attr['title'] .'</p>';
			$html .= '<div class="progress">';
				$html .= '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'. $attr['percent'] .'" aria-valuemin="0" aria-valuemax="100" style="width: '. $attr['percent'] .'%">';
					$html .= '<span class="sr-only">'. $attr['percent'] .'% Complete (success)</span>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_progressbar', 'lz_vc_progressbar' );


vc_map( array (
	'base' 			=> 'lz_progressbar',
	'name' 			=> __('LZ Progress Bar', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'percent',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Percent', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
