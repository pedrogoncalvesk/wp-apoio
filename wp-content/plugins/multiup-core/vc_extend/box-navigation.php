<?php
if( ! function_exists( 'lz_vc_box_navigation' ) ) {
	function lz_vc_box_navigation( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'subtitle'					    => '',
			'color'							=> '',
			'buttontext'					=> '',
			'buttonlink'					=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['subtitle'] = isset($attr['subtitle']) ? $attr['subtitle'] : '';
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '';
		$attr['buttontext'] = isset($attr['buttontext']) ? $attr['buttontext'] : '';
		$attr['buttonlink'] = isset($attr['buttonlink']) ? $attr['buttonlink'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="rst-page-shortdescription ' . rst_class($class) . ' ">';
			$html .= '<div class="container">';
				$html .= '<h3 style="color:'. $attr['color'] .'">'. $attr['title'] .'</h3>';
				$html .= '<p style="color:'. $attr['color'] .'">'. $attr['subtitle'] .'</p>';
				$html .= '<a class="btn btn-success" href="'. $attr['buttonlink'] .'">'. $attr['buttontext'] .'</a>';
				$html .= '<div class="clear"></div>';
			$html .= '</div>';
		$html .= '</div>';
		
		
		return $html;
	}
}
add_shortcode( 'lz_box_navigation', 'lz_vc_box_navigation' );


vc_map( array (
	'base' 			=> 'lz_box_navigation',
	'name' 			=> __('LZ Navigation', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'subtitle',
			'type' 			=> 'textarea',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Text color"),
            "param_name" => "color",
            "value" => '#FFFFFF', //Default Red color
            "description" => __( "Choose text color", "my-text-domain" )
        ),
		array (
			'param_name' 	=> 'buttontext',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Button text', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'buttonlink',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Button link', 'mfn-opts'),
			'admin_label'	=> true,
		),
		
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
