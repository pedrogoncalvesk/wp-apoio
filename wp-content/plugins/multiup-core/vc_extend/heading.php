<?php
if( ! function_exists( 'mu_vc_group_title' ) ) {
	function mu_vc_group_title( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'							=> '',
			'title_tag'						=> '',
			'title_size'					=> '',
			'subtitle'						=> '',
			'subtitle_size'					=> '',
			'color'							=> '',
			'enable_animate'				=> '',
			'animate'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['title_tag'] = isset($attr['title_tag']) ? $attr['title_tag'] : 'h3';
		$attr['title_size'] = isset($attr['title_size']) ? $attr['title_size'] : '33px';
		$attr['subtitle'] = isset($attr['subtitle']) ? $attr['subtitle'] : '';
		$attr['subtitle_size'] = isset($attr['subtitle_size']) ? $attr['subtitle_size'] : '18px';
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '#212f44';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
			
		$html .= '<div class="rst-group-title '. rst_class($class) .'" >';
			$html .= '<'. $attr['title_tag'] .'>
						<span style="color:'. $attr['color'] .';font-size:'. $attr['title_size'] .'">'. $attr['title'] .'</span>
						</'. $attr['title_tag'] .'>';
			if ( $attr['subtitle'] ) {
				$html .= '<p style="color:'. $attr['color'] .';font-size:'. $attr['subtitle_size'] .'">'. $attr['subtitle'] .'</p>';
			}
		$html .= '</div>';
		return $html;
		
	}
}
add_shortcode( 'mu_group_title', 'mu_vc_group_title' );


vc_map( array (
	'base' 			=> 'mu_group_title',
	'name' 			=> __('LZ Heading', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'heading' 		=> __('Heading', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'title_tag',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Heading Tag', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("h2", LANGUAGE_ZONE) => "h2", 
							__("h3", LANGUAGE_ZONE) => "h3",
							__("h4", LANGUAGE_ZONE) => "h4",
							__("h5", LANGUAGE_ZONE) => "h5",
							__("h6", LANGUAGE_ZONE) => "h6",
							__("div", LANGUAGE_ZONE) => "div",
			),
		),
		array (
			'param_name' 	=> 'title_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Heading Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '33px'
		),
		array (
			'param_name' 	=> 'subtitle',
			'type' 			=> 'textarea',
			'heading' 		=> __('Description', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'subtitle_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Description Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '18px'
		),
		array(
            "type" => "colorpicker",
            "heading" => __( "Text color"),
            "param_name" => "color",
            "description" => __( "Choose text color", "my-text-domain" )
        ),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',
		),
	)
));
