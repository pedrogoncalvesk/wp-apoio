<?php
if( ! function_exists( 'lz_vc_button' ) ) {
	function lz_vc_button( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'text'								=> '',
			'link'								=> '',
			'target_blank'						=> '',
			'style'								=> '',
			'hover'								=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'					=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['text'] = isset($attr['text']) ? $attr['text'] : '';
		$attr['link'] = isset($attr['link']) ? $attr['link'] : '';
		$attr['target_blank'] = isset($attr['target_blank']) ? $attr['target_blank'] : false;
		$attr['style'] = isset($attr['style']) ? $attr['style'] : 'btn-lg';
		$attr['hover'] = isset($attr['hover']) ? $attr['hover'] : false;
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$link = $attr['link'] ? ' href="'. esc_url($attr['link']) .'"' : '';
		$hover = $attr['hover'] ? 'btn2' : '';
		$target_blank = $attr['target_blank'] ? ' target="_blank"' : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		$class[] = $attr['style'];
		$class[] = $hover;
		
		$html = '<a '. $link .' '. $target_blank .' class="btn btn-success '. rst_class($class) .'">'. $attr['text'] .'</a>';
		
		return $html;
	}
}
add_shortcode( 'lz_button', 'lz_vc_button' );

vc_map( array (
	'base' 			=> 'lz_button',
	'name' 			=> __('LZ Button', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'text',
			'type' 			=> 'textfield',
			'heading' 		=> __('Text', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'link',
			'type' 			=> 'textfield',
			'heading' 		=> __('Link URL', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'target_blank',
			'type' 			=> 'dropdown',
			'heading' 		=> __('Open link in', 'mfn-opts'),
			'admin_label'	=> true,
			"value" 		=> array(
							__("Same window", LANGUAGE_ZONE) => "false", 
							__("New window", LANGUAGE_ZONE) => "true"
			),
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("Style", LANGUAGE_ZONE),
			"param_name" 	=> "style",
			"admin_label" 	=> true,
			"value" 		=> array(
							__("Big", LANGUAGE_ZONE) => "btn-lg", 
							__("Medium", LANGUAGE_ZONE) => "btn-medium",
							__("Small", LANGUAGE_ZONE) => "btn-sm"
			),
		),
		array (
			'param_name' 	=> 'hover',
			'type' 			=> 'checkbox',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Hover Animate', 'mfn-opts'),
			'admin_label'	=> true,                                                  
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
