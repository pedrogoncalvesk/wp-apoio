<?php
if( ! function_exists( 'lz_vc_pricing' ) ) {
	function lz_vc_pricing( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'packagename'					=> '',
			'pricing'					    => '',
			'sale'							=> '',
			'per'							=> '',
			'options'						=> '',
			'packagelink'					=> '',
			'display'					    => '',
			'margin_bottom'					=> '',
			'enable_animate'				=> '',
			'animate'						=> '',
			'extraclassname'				=> '',
			
			
		), $attr));
		
		$html = '';
		
		$attr['packagename'] = isset($attr['packagename']) ? $attr['packagename'] : '';
		$attr['pricing'] = isset($attr['pricing']) ? $attr['pricing'] : '';
		$attr['sale'] = isset($attr['sale']) ? $attr['sale'] : '';
		$attr['per'] = isset($attr['per']) ? $attr['per'] : '';
		$attr['options'] = isset($attr['options']) ? $attr['options'] : '';
		$attr['packagelink'] = isset($attr['packagelink']) ? $attr['packagelink'] : '';
		$attr['display'] = isset($attr['display']) ? $attr['display'] : '';
		$attr['margin_bottom'] = isset($attr['margin_bottom']) ? $attr['margin_bottom'] : '30';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$text = trim($attr['options']);
		$textAr = explode("\n", $text);
		$textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind

		$html .= '<div class="' . rst_class($class) . ' ' . $attr['display'] . ' ">';	
			$html .= '<div class="rst-inner-pricing" style="margin-bottom: '. absint($attr['margin_bottom']) .'px">';
				$html .= $attr['sale'] == '' ? '' : '<div class="rst-sale">'. $attr['sale'] .'</div>';
				$html .= '<div class="rst-header-pack">';
					$html .= '<span>'. $attr['packagename'] .'</span>';
					$html .= '<p><sup>$</sup>'. $attr['pricing'] .'<span>/'. $attr['per'] .'</span></p>';
				$html .= '</div>';
				$html .= '<div class="rst-detail-pack">';
					$html .= '<ul>';
						foreach ($textAr as $line) {
							$html .= '<li><p><i class="fa fa-check"></i> '.$line.'</p></li>';
						} 
					$html .= '</ul>';
				$html .= '</div>';
				$html .= '<a class="btn " href="'. $attr['packagelink'] .'">Continue</a>';
			$html .= '</div>';
		$html .= '</div>';
				
		return $html;
	}
}
add_shortcode( 'lz_pricing', 'lz_vc_pricing' );


vc_map( array (
	'base' 			=> 'lz_pricing',
	'name' 			=> __('LZ Pricing', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'packagename',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Package Name', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'pricing',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Pricing', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'sale',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Sale', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'per',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Per / ', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'options',
			'type' 			=> 'textarea',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Package Options', 'mfn-opts'),
			'admin_label'	=> true,
			"description"   => "* One option per line.",
		),
		array (
			'param_name' 	=> 'packagelink',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Link', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
			"type" => "dropdown",
			"heading" => __("Display", LANGUAGE_ZONE),
			"param_name" => "display",
			"admin_label" => true,
			"value" => array(__("Vertical", LANGUAGE_ZONE) => 'rst-style-none', __("Horizontal", LANGUAGE_ZONE) => "rst-home2"),
		),
		array(
			"type" => "textfield",
			"heading" => __("Margin Bottom", LANGUAGE_ZONE),
			"param_name" => "margin_bottom",
			"admin_label" => true,
			"value" => "30",
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
