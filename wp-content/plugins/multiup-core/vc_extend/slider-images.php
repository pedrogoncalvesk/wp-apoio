<?php
if( ! function_exists( 'lz_vc_slider_images' ) ) {
	function lz_vc_slider_images( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'images'						=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['images'] = isset($attr['images']) ? $attr['images'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$pieces = explode(",", $attr['images']);
		
		$html .= '<div class="owl-carousel owl-blogsingle ' . $attr['extraclassname'] . ' ">';
			foreach ( $pieces as $item ) 
			{
				$img = wp_get_attachment_image_src($item, '');
				$imgSrc = $img[0];
				$html .= '<img src="'.$imgSrc.'" alt="" />';
			}
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_slider_images', 'lz_vc_slider_images' );

vc_map( array (
	'base' 			=> 'lz_slider_images',
	'name' 			=> __('LZ Slider Images', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		 array (
			'param_name' 	=> 'images',
			'type' 			=> 'attach_images',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Add Images', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
