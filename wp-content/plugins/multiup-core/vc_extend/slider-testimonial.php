<?php
if( ! function_exists( 'lz_vc_slider_testimonial' ) ) {
	function lz_vc_slider_testimonial( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'loop'							=> '',
			'color'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['loop'] = isset($attr['loop']) ? $attr['loop'] : '';
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '#333333';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		
		$loop = $attr['loop'] .'|post_type:testimonials';
		$query = vc_build_loop_query($loop,get_the_ID());
		if( isset($query[1]) && is_object($query[1]) ) $wp_query = $query[1];
		if( $wp_query ) {
			
			$html .= '<div class="rst-testimonial-slider ' . $attr['extraclassname'] . '">';
				$html .= '<div class="container">';
					$html .= '<div class="owl-carousel owl-testimonialslider">';
						while ( $wp_query->have_posts() ) {
							$wp_query->the_post();
							$html .= '<div>';
								$html .= '<p style="color:'. $attr['color'] .'">'. rs::getField('rst_testimonial_text',get_the_ID()) .'</p>';
								$html .= '<img src="'.wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ).'" alt="" />';
								$html .= '<cite style="color:'. $attr['color'] .'">'. rs::getField('rst_testimonial_name',get_the_ID()) .'</cite>';
							$html .= '</div>';
						}
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
		
		}
		wp_reset_query();
		return $html;
	}
}
add_shortcode( 'lz_slider_testimonial', 'lz_vc_slider_testimonial' );

vc_map( array (
	'base' 			=> 'lz_slider_testimonial',
	'name' 			=> __('LZ Slider Testimonial', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array(
            "type" => "loop",
            "heading" => __("Slider content", LANGUAGE_ZONE),
            "param_name" => "loop",
            'settings' => array(
                'post_type' => array('hidden' => true,'value' =>'testimonials'),
                'size' => array('hidden' => false, 'value' => 10),
                'order_by' => array('value' => 'date'),
				'categories' => array('hidden' => true),
				'tags' => array('hidden' => true),
				'tax_query' => array('hidden' => true),
				'authors' => array('hidden' => true),
            ),
            "description" => __("Create WordPress slider testimonial, to populate content from your site.", LANGUAGE_ZONE)
        ),
		array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Text color"),
            "param_name" => "color",
            "value" => '#333333', //Default Red color
            "description" => __( "Choose text color", "my-text-domain" )
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
