<?php
if( ! function_exists( 'lz_vc_page_blog' ) ) {
	function lz_vc_page_blog( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'loop'								=> '',
			'excerpt_length'					=> '',
			'hide_pagenavi'						=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'					=> '',
			
		), $attr));
		
		ob_start();
		
		global $wp_query;
		$attr['loop'] = isset($attr['loop']) ? $attr['loop'] : '';
		$attr['excerpt_length'] = isset($attr['excerpt_length']) ? $attr['excerpt_length'] : 0;
		$attr['hide_pagenavi'] = isset($attr['hide_pagenavi']) ? $attr['hide_pagenavi'] : false;
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		$html = '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$loop = $attr['loop'] .'|post_type:post';
		$query = vc_build_loop_query($loop,get_the_ID());
		
		$array_query = array();
		if( isset($query[0]) && is_array($query[0]) ) $array_query = $query[0];
		global $rst_blog;
		$rst_blog['excerpt_length'] = $attr['excerpt_length'];
		
		$args = array(
			'paged' => max( get_query_var( 'paged' ), get_query_var( 'page' ))
		);
		
		$array_query = array_merge( $array_query, $args );
		
		$wp_query = new WP_Query( $array_query );
		
		if( $wp_query->have_posts() ) {
			
			echo '<section class="rst-main-page '. rst_class($class) .'" id="rst-blog-page">';
			
				echo '<div class="rst-blog-container">';
				
					while ($wp_query->have_posts() ) : $wp_query->the_post(); 

						echo rst_get_template_part( 'content', get_post_format() );
						
					endwhile;
					
					if( !$attr['hide_pagenavi'] ) {
						echo '<div class="rst-page">';
							rst_the_posts_navigation();
						echo '</div>';
					}
					
				echo '</div>';
				
			echo '</section>';
			
			$html = ob_get_contents();
			ob_end_clean();
		}
		wp_reset_query();
		return $html;
	}
}
add_shortcode( 'lz_page_blog', 'lz_vc_page_blog' );

vc_map( array (
	'base' 			=> 'lz_page_blog',
	'name' 			=> __('LZ Blog Page', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array(
            "type" => "loop",
            "heading" => __("Query Posts", LANGUAGE_ZONE),
            "param_name" => "loop",
            'settings' => array(
                'post_type' => array('hidden' => true,'value' =>'post'),
                'order_by' => array('value' => 'date'),
            ),
        ),
		array (
			'param_name' 	=> 'excerpt_length',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Excerpt Length', 'mfn-opts'),
			'admin_label'	=> true,                                              
		),
		array (
			'param_name' 	=> 'hide_pagenavi',
			'type' 			=> 'checkbox',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Hidden Pagenavi', 'mfn-opts'),
			'admin_label'	=> true,                                              
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
