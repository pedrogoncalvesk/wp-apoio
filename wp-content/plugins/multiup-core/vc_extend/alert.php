<?php
if( ! function_exists( 'lz_vc_alert' ) ) {
	function lz_vc_alert( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'alerttext'								=> '',
			'alertstyle'							=> '',
			'enable_animate'						=> '',
			'animate'								=> '',
			'extraclassname'						=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['alerttext'] = isset($attr['alerttext']) ? $attr['alerttext'] : '';
		$attr['alertstyle'] = isset($attr['alertstyle']) ? $attr['alertstyle'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		$class[] = $attr['alertstyle'];
		
		$html .= '<div role="alert" class="alert alert-dismissible '. rst_class($class) .'">';
			$html .= '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><i class="fa fa-close"></i></button>';
			$html .= $attr['alerttext'];
		$html .= '</div>';
		return $html;
	}
}
add_shortcode( 'lz_alert', 'lz_vc_alert' );

vc_map( array (
	'base' 			=> 'lz_alert',
	'name' 			=> __('LZ Alert', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'alerttext',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Text', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("Style", LANGUAGE_ZONE),
			"param_name" 	=> "alertstyle",
			"admin_label" 	=> true,
			"value" 		=> array(
							__("None", LANGUAGE_ZONE) => "", 
							__("Success", LANGUAGE_ZONE) => "alert-success", 
							__("Danger", LANGUAGE_ZONE) => "alert-danger",
							__("Warning", LANGUAGE_ZONE) => "alert-warning",
							__("Info", LANGUAGE_ZONE) => "alert-info",
			),
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
