<?php 

add_shortcode_param( 'terms', 'ub_terms_settings_field', MULTIUP_PLUGIN_URL. '/vc_extend/js/vc_extend_checkbox.js' );
function ub_terms_settings_field( $settings, $value ) {
	$html = '';
	$taxonomies = isset($settings['taxonomies']) ? $settings['taxonomies'] : 'category'; // name taxonomy
	$hide_empty = isset($settings['hide_empty']) ? $settings['hide_empty'] : '0'; // 0 or 1
	$terms = get_terms( $taxonomies, array(
		'orderby'    => 'term_group',
		'order' => 'ASC',
		'hide_empty' => $hide_empty,
		'parent' => 0
	));
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		$html .= '<div class="ub-term-list">';
		$html .= '<input type="hidden" name="'. esc_attr( $settings['param_name'] ) .'" class="wpb_vc_param_value wpb-textinput ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" value="' . esc_attr( $value ) . '" ub-data-name="'. $settings['param_name'] .'" />';
		$html .= '<ul>';
		$value = explode(',',$value);
		foreach ( $terms as $key=>$term ) {
			$html .= '<li><label><input type="checkbox" '. (in_array((string)$term->slug, $value) ? 'checked="checked"' : '') .' value="'. $term->slug .'" ub-data-input="'. $settings['param_name'] .'"> '. $term->name .'</label>'. ub_get_terms_tree($term->term_id,$taxonomies,$value,$settings['param_name']) .'</li>';
		}
		$html .= '</ul>';
		$html .= '</div>';
	}
	else {
		$html = 'Sorry, but nothing matched your search terms.';
	}
	return $html;
}
function ub_get_terms_tree($term_id,$taxonomy,$value,$name) {
	$html = '';
	$terms = get_terms( $taxonomy, array(
		'orderby'    => 'term_group',
		'order' => 'ASC',
		'parent'	=> $term_id,
		'hide_empty' => 0
	));
	if( $terms ){
		$html .= '<ul class="sub-menu">';
			foreach( $terms as $term )
				$html .= '<li><label><input type="checkbox" '. (in_array((string)$term->slug, $value) ? 'checked="checked"' : '') .' value="'. $term->slug .'" ub-data-input="'. $name .'"> '. $term->name .'</label>'. ub_get_terms_tree($term->slug,$taxonomy,$value,$name) .'</li>';
		$html .= '</ul>';
	}
	return $html;
		
}