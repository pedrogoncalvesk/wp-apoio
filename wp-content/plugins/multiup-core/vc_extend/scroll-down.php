<?php
if( ! function_exists( 'lz_vc_scolldown' ) ) {
	function lz_vc_scolldown( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'color'							=> '',
		), $attr));
			
		$html = '';
		
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '#fff';
		
		$html = '<div class="rst-scroll-down">';
			$html .= '<a href="#"><i style="color:'.$attr['color'].'" class="fa fa-chevron-down"></i></a>';
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_scrolldown', 'lz_vc_scolldown' );

vc_map( array (
	'base' 			=> 'lz_scrolldown',
	'name' 			=> __('LZ Scroll Down', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		 array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Icon color"),
            "param_name" => "color",
            "value" => '#FFFFFF', //Default Red color
            "description" => __( "Choose Icon color", "my-text-domain" )
        ),
	)
));
