<?php
if( ! function_exists( 'lz_vc_slider_partners' ) ) {
	function lz_vc_slider_partners( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'loop'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['loop'] = isset($attr['loop']) ? $attr['loop'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$loop = $attr['loop'] .'|post_type:partners';
		$query = vc_build_loop_query($loop,get_the_ID());
		if( isset($query[1]) && is_object($query[1]) ) $wp_query = $query[1];
		if( $wp_query ) {
			$html .= '<div class="owl-carousel rst-partner-carousel ' . $attr['extraclassname'] . ' ">';
				while ( $wp_query->have_posts() ) {
					$wp_query->the_post();
						$html .= '<a href="'. esc_url(rs::getField('rst_partner_link')) .'"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ).'" alt="" /></a>';
				}
			$html .= '</div>';
		}
		
		return $html;
	}
}
add_shortcode( 'lz_slider-partners', 'lz_vc_slider_partners' );

vc_map( array (
	'base' 			=> 'lz_slider-partners',
	'name' 			=> __('LZ Slider Partners', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		 array(
            "type" => "loop",
            "heading" => __("Slider content", LANGUAGE_ZONE),
            "param_name" => "loop",
            'settings' => array(
                'post_type' => array('hidden' => true,'value' =>'partners'),
                'size' => array('hidden' => false, 'value' => 10),
                'order_by' => array('value' => 'date'),
				'categories' => array('hidden' => true),
				'tags' => array('hidden' => true),
				'tax_query' => array('hidden' => true),
				'authors' => array('hidden' => true),
            ),
            "description" => __("Create WordPress slider partners, to populate content from your site.", LANGUAGE_ZONE)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
