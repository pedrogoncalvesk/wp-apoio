<?php
if( ! function_exists( 'lz_vc_accordions' ) ) {
	function lz_vc_accordions( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'title'								=> '',
			'acccontent'						=> '',
			'show'								=> '',
			'accstyle'							=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'					=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['acccontent'] = isset($attr['acccontent']) ? $attr['acccontent'] : '';
		$attr['accstyle'] = isset($attr['accstyle']) ? $attr['accstyle'] : 1;
		$attr['show'] = isset($attr['show']) ? $attr['show'] : false;
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		$show = $attr['show'] ? 'in' : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		$class[] = 'rst-accordions-style'.$attr['accstyle'];
		
		$rstkey =  uniqid();
		
		$html .= '<div class="panel panel-default '. rst_class($class) .'">';
			$html .= '<div role="tab" class="panel-heading">';
				$html .= '<h4 class="panel-title">';
				$html .= '<a aria-controls="'.$rstkey.'" aria-expanded="true" href="#'.$rstkey.'" data-parent="" data-toggle="collapse" class="">';
					$html .= $attr['title'];
				$html .= '</a>';
				$html .= '</h4>';
			$html .= '</div>';
			$html .= '<div aria-labelledby="" role="tabpanel" class="panel-collapse collapse '. $show .'" id="'.$rstkey.'" style="" aria-expanded="true">';
				$html .= '<div class="panel-body">';
					$html .= $attr['acccontent'];
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_accordions', 'lz_vc_accordions' );

vc_map( array (
	'base' 			=> 'lz_accordions',
	'name' 			=> __('LZ Accordions', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,                                                   
		),
		array (
			'param_name' 	=> 'acccontent',
			'type' 			=> 'textarea',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Content', 'mfn-opts'),
			'admin_label'	=> true,                                                  
		),
		array (
			'param_name' 	=> 'show',
			'type' 			=> 'checkbox',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Show Content', 'mfn-opts'),
			'admin_label'	=> true,                                                  
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("Style", LANGUAGE_ZONE),
			"param_name" 	=> "accstyle",
			"admin_label" 	=> true,
			"value" 		=> array(__("Border", LANGUAGE_ZONE) => "1", __("Only Border Bottom", LANGUAGE_ZONE) => "2"),
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
