<?php
if( ! function_exists( 'lz_vc_gallery' ) ) {
	function lz_vc_gallery( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'images'					=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'			=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['images'] = isset($attr['images']) ? $attr['images'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$pieces = explode(",", $attr['images']);
		$rstkey =  uniqid();
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		if( is_array($pieces) && sizeof($pieces) > 0 ) {
			$html .= '<div class="rst-gallery ' . rst_class($class) . ' ">';
				foreach ( $pieces as $item ) 
				{
					$img = wp_get_attachment_url($item);
					$html .= '<a data-fancybox-group="'.$rstkey.'" href="'.$img.'" class="fancybox"><img alt="" src="'. bfi_thumb($img,array('width'=>270,'height'=>270)) .'"></a>';
				}
				$html .= '<div class="clear"></div>';
			$html .= '</div>';
		}
		
		return $html;
	}
}
add_shortcode( 'lz_gallery', 'lz_vc_gallery' );

vc_map( array (
	'base' 			=> 'lz_gallery',
	'name' 			=> __('LZ Gallery', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'images',
			'type' 			=> 'attach_images',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Add Images', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
