<?php
if( ! function_exists( 'lz_vc_team' ) ) {
	function lz_vc_team( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'image'					=> '',
			'name'					=> '',
			'job'					=> '',
			'information'			=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['image'] = isset($attr['image']) ? $attr['image'] : '';
		$attr['name'] = isset($attr['name']) ? $attr['name'] : '';
		$attr['job'] = isset($attr['job']) ? $attr['job'] : '';
		$attr['information'] = isset($attr['information']) ? $attr['information'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$img = wp_get_attachment_image_src($attr['image'], '');
		if( is_array($img) ) {
			$imgSrc = $img[0];
		}
		else {
			$imgSrc = '';
		}
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="rst-team '. rst_class($class) .'">';
			$html .= '<div class="rst-teamimg">';
				if($imgSrc) {
					$html .= '<img src="'. $imgSrc .'" alt="" />';
				}
				$html .= '<div class="rst-teaminfo-hidden">';
					$html .= '<div class="rst-infoscroll">';
						$html .= '<span>ABOUT</span>';
						$html .= '<p>'. $attr['information'] .'</p>';
					$html .= '</div>';
				$html .= '</div>';
				
			$html .= '</div>';
			$html .= '<div class="rst-teaminfo">';
				$html .= '<p>';
					$html .= '<cite>'. $attr['name'] .'</cite>';
					$html .= '<span>'. $attr['job'] .'</span>';
				$html .= '</p>';
				$html .= '<i class="fa fa-times rst-teaminfo-close"></i>';
			$html .= '</div>';
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_team', 'lz_vc_team' );


vc_map( array (
	'base' 			=> 'lz_team',
	'name' 			=> __('LZ Team', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'image',
			'type' 			=> 'attach_image',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Image', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'name',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Name', 'mfn-opts'),
			'admin_label'	=> true,      
		),
		array (
			'param_name' 	=> 'job',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Job', 'mfn-opts'),
			'admin_label'	=> true,        
		),
		array (
			'param_name' 	=> 'information',
			'type' 			=> 'textarea',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Information', 'mfn-opts'),
			'admin_label'	=> true,        
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
