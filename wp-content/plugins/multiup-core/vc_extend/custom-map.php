<?php
if( ! function_exists( 'lz_vc_custom_map' ) ) {
	function lz_vc_custom_map( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'address'					=> '',
			'mapzoom'					=> '',
			'label'						=> '',
			'icon'						=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'			=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['address'] = isset($attr['address']) ? $attr['address'] : '';
		$attr['mapzoom'] = isset($attr['mapzoom']) ? $attr['mapzoom'] : '';
		$attr['label'] = isset($attr['label']) ? $attr['label'] : '';
		$attr['icon'] = isset($attr['icon']) ? $attr['icon'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$img = wp_get_attachment_image_src($attr['icon'], 'small');
		$imgSrc = '';
		if( is_array($img) ) {
			$imgSrc = $img[0];
		}
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="rst-contact-map ' . rst_class($class) . ' ">';
		$html .=	'<div id="cd-google-map" data-address="'. $attr['address'] .'" data-zoom="'. $attr['mapzoom'] .'" data-label="'. $attr['label'] .'" data-icon="'. $imgSrc .'" >';
		$html .=		'<div id="google-container"></div>';
		$html .=	'</div>';
		$html .= '</div>';
		
		
		return $html;
	}
}
add_shortcode( 'lz_custom_map', 'lz_vc_custom_map' );


vc_map( array (
	'base' 			=> 'lz_custom_map',
	'name' 			=> __('LZ Custom google map', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'address',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Address - latitude & longitude', 'mfn-opts'),
			'description'   => 'Go to http://www.latlong.net and put the name of a place, city, state, or address, or click the location on the map to get lat long coordinates <br/>eg:33.8977605,-118.4065804',                                                     
			'type' 			=> 'textfield',
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'mapzoom',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Map Zoom', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'label',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Map label', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'icon',
			'type' 			=> 'attach_image',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Location Icon', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
