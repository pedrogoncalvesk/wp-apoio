<?php
if( ! function_exists( 'lz_vc_group_blog' ) ) {
	function lz_vc_group_blog( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'loop'							=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['loop'] = isset($attr['loop']) ? $attr['loop'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$loop = $attr['loop'] .'|post_type:post';
		$query = vc_build_loop_query($loop,get_the_ID());
		if( isset($query[1]) && is_object($query[1]) ) $wp_query = $query[1];
		if( $wp_query ) {
		
			$html .= '<div class="rst-blog ' . rst_class($class) . ' ">';
				$html .= '<div class="container">';
					$html .= '<div class="row">';
						
						while ( $wp_query->have_posts() ) {
						$wp_query->the_post();
						
							$html .= '<div class="col-sm-4">';
								$html .= '<article>';
									$html .= '<div class="rst-blog-excerpt rst-blog-info">';
										$html .= '<time datetime="'. get_the_date('Y-F-j') .'">'.get_the_date('j F Y').'</time>';
										$html .= '<p class="rst-blog-title"><a href="'. get_permalink() .'">'. get_the_title() .'</a></p>';
										$html .= '<p>'.get_excerpt_by_id(get_the_ID(),20).'</p>';
									$html .= '</div>';	
								$html .= '</article>';
							$html .= '</div>';
						
						}	
						
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
			
		}
		
		return $html;
	}
}
add_shortcode( 'lz_group_blog', 'lz_vc_group_blog' );

vc_map( array (
	'base' 			=> 'lz_group_blog',
	'name' 			=> __('LZ Blog', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		 array(
            "type" => "loop",
            "heading" => __("Group Options", LANGUAGE_ZONE),
            "param_name" => "loop",
            'settings' => array(
                'post_type' => array('hidden' => true,'value' =>'post'),
                'size' => array('hidden' => false, 'value' => 10),
                'order_by' => array('value' => 'date'),
				'categories' => array('hidden' => true),
				'tags' => array('hidden' => true),
				'tax_query' => array('hidden' => true),
				'authors' => array('hidden' => true),
            ),
        ),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
