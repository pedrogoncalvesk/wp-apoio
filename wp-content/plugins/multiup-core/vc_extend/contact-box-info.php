<?php
if( ! function_exists( 'mu_vc_boxinfo' ) ) {
	function mu_vc_boxinfo( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'icon'						=> '',
			'title'						=> '',
			'subtitle'					=> '',
			'tel'						=> '',
			'email'						=> '',
			'enable_animate'					=> '',
			'animate'							=> '',
			'extraclassname'				=> '',
			
		), $attr));
		
		$html = '';
		
		$attr['icon'] = isset($attr['icon']) ? $attr['icon'] : '';
		$attr['title'] = isset($attr['title']) ? $attr['title'] : '';
		$attr['subtitle'] = isset($attr['subtitle']) ? $attr['subtitle'] : '';
		$attr['tel'] = isset($attr['tel']) ? $attr['tel'] : '';
		$attr['email'] = isset($attr['email']) ? $attr['email'] : '';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="rst-contact-box ' . rst_class($class) . ' ">';
			$html .= '<i class="'. $attr['icon'] .'"></i>';
			$html .= '<h2>'. $attr['title'] .'</h2>';
			$html .= '<p>'. $attr['subtitle'] .'</p>';
			$html .= '<a href="tel:'. $attr['tel'] .'">'. $attr['tel'] .'</a>';
			$html .= '<a href="mailto:'. $attr['email'] .'">'. $attr['email'] .'</a>';
		$html .= '</div>';
		
		
		return $html;
	}
}
add_shortcode( 'mu_contact_box', 'mu_vc_boxinfo' );


vc_map( array (
	'base' 			=> 'mu_contact_box',
	'name' 			=> __('LZ Contact Box', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'icon',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Icon', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'title',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Title', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'subtitle',
			'type' 			=> 'textarea',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Sub Title', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'tel',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Tel', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'email',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Email', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'taxonomies' 	=> 'category',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
