<?php
if( ! function_exists( 'lz_vc_group_funfact' ) ) {
	function lz_vc_group_funfact( $attr, $content = null )
	{
		extract(shortcode_atts(array(
			'icon'							=> '',
			'icon_color'					=> '',
			'icon_size'						=> '',
			'number'						=> '',
			'number_size'					=> '',
			'text'							=> '',
			'text_size'						=> '',
			'color'							=> '',
			'textalign'						=> '',
			'enable_animate'				=> '',
			'animate'						=> '',
			'extraclassname'			    => '',
			
		), $attr));
		
		$html = '';
		
		$attr['icon'] = isset($attr['icon']) ? $attr['icon'] : '';
		$attr['icon_color'] = isset($attr['icon_color']) ? $attr['icon_color'] : '#2ecc71';
		$attr['icon_size'] = isset($attr['icon_size']) ? $attr['icon_size'] : '35px';
		$attr['number'] = isset($attr['number']) ? $attr['number'] : '';
		$attr['number_size'] = isset($attr['number_size']) ? $attr['number_size'] : '45px';
		$attr['text'] = isset($attr['text']) ? $attr['text'] : '';
		$attr['text_size'] = isset($attr['text_size']) ? $attr['text_size'] : '16px';
		$attr['color'] = isset($attr['color']) ? $attr['color'] : '';
		$attr['textalign'] = isset($attr['textalign']) ? $attr['textalign'] : 'center';
		$attr['enable_animate'] = isset($attr['enable_animate']) ? $attr['enable_animate'] : false;
		$attr['animate'] = isset($attr['animate']) ? $attr['animate'] : '';
		$attr['extraclassname'] = isset($attr['extraclassname']) ? $attr['extraclassname'] : '';
		
		$class = array();
		
		if( $attr['enable_animate'] ) {
			$class[] = 'wow';
			$class[] = $attr['animate'];
		}
		
		$class[] = $attr['extraclassname'];
		
		$html .= '<div class="rst-funfacts ' . rst_class($class) . ' " style="text-align:'.$attr['textalign'].'">';
			if( $attr['icon'] ){
				$html .= '<i style="color:'. $attr['icon_color'] .';font-size:'. $attr['icon_size'] .'" class="'.$attr['icon'].'"></i>';
			}
			if( $attr['number'] ) {
				$html .= '<p class="counter" style="color:'. $attr['color'] .';font-size:'. $attr['number_size'] .'">'.$attr['number'].'</p>';
			}
			if( $attr['text'] ) {
				$html .= '<span style="color:'. $attr['color'] .';font-size:'. $attr['text_size'] .'">'.$attr['text'].'</span>';
			} 
		$html .= '</div>';
		
		return $html;
	}
}
add_shortcode( 'lz_group_funfact', 'lz_vc_group_funfact' );

vc_map( array (
	'base' 			=> 'lz_group_funfact',
	'name' 			=> __('LZ Count to', 'mfn-opts'),
	'category' 		=> __('Content', 'mfn-opts'),
	'icon' 			=> 'mu_icon_shortcode',
	'params' 		=> array (
		array (
			'param_name' 	=> 'icon',
			'type' 			=> 'rst_icons',
			'heading' 		=> __('Icon', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Icon color"),
            "param_name" => "icon_color",
            "value" => '#2ecc71', //Default Red color
            "description" => __( "Choose icon color", "my-text-domain" )
        ),
		array (
			'param_name' 	=> 'icon_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Icon Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '35px'
		),
		array (
			'param_name' 	=> 'number',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'number_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Number Size', 'mfn-opts'),
			'admin_label'	=> true,
			'value'			=> '45px'
		),
		array (
			'param_name' 	=> 'text',
			'type' 			=> 'textfield',
			'heading' 		=> __('Heading', 'mfn-opts'),
			'admin_label'	=> true,
		),
		array (
			'param_name' 	=> 'text_size',
			'type' 			=> 'textfield',
			'heading' 		=> __('Heading Size', 'mfn-opts'),
			"value" 		=> '16px',
			'admin_label'	=> true,
		),
		array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Heading color"),
            "param_name" => "color",
            "value" => '#212f44', //Default Red color
            "description" => __( "Choose text color", "my-text-domain" )
        ),
		array(
			"type" => "dropdown",
			"heading" => __("Text Align", LANGUAGE_ZONE),
			"param_name" => "textalign",
			"admin_label" => true,
			"value" => array(__("Left", LANGUAGE_ZONE) => 'left', __("Right", LANGUAGE_ZONE) => "right", __("Center", LANGUAGE_ZONE) => "center"),
		),
		array(
            "type" => "checkbox",
            "heading" => __( "Animate"),
            "param_name" => "enable_animate",
            "description" => __( "Enable animate", "my-text-domain" ),
			'admin_label'	=> true,     
        ),
		array(
            "type" => "animate",
            "heading" => __( "Animate"),
            "param_name" => "animate",
            "description" => __( "Visit <a target='_blank' href='https://daneden.github.io/animate.css/'>Website Animate.css</a> view animate", "my-text-domain" ),
			'admin_label'	=> true, 
			"dependency" => array(
				"element" => "enable_animate",
				"value" => array(
					'true'
				)
			)
        ),
		array (
			'param_name' 	=> 'extraclassname',
			'type' 			=> 'textfield',
			'heading' 		=> __('Extra Class Name', 'mfn-opts'),
			'admin_label'	=> true,
			'description'   => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.',                                                     
		),
	)
));
