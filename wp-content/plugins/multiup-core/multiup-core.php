<?php
/**
 * Plugin Name: MultiUp Core
 * Description: Setup MultiUp Core.
 * Version: 1.0.0
 * License: A "Slug" license name e.g. GPL2
 */
 
	define( 'MULTIUP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
	
	define( 'MULTIUP_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );
	
	function rst_multiup_backend_enqueue() {
		wp_enqueue_style( 'rst_multiup_style', MULTIUP_PLUGIN_URL .'/style.css' );
		wp_enqueue_style( 'css-fontstrokehelper', MULTIUP_PLUGIN_URL . '/fonts/fontstroke/pe-icon-7-stroke/css/helper.css' );
		wp_enqueue_style( 'css-fontstroke', MULTIUP_PLUGIN_URL . '/fonts/fontstroke/pe-icon-7-stroke/css/pe-icon-7-stroke.css' ); 
	}
	add_action( 'admin_enqueue_scripts', 'rst_multiup_backend_enqueue', 10000 );
	
	add_action( 'init', 'register_partner_post_type' ); 

	if ( ! function_exists( 'register_partner_post_type' ) ) {
		function register_partner_post_type () {
			$labels = array(
				'name'                => _x( 'partners', 'Post Type General Name', 'text_domain' ),
				'singular_name'       => _x( 'partner', 'Post Type Singular Name', 'text_domain' ),
				'menu_name'           => __( 'Partners', 'text_domain' ),
				'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
				'all_items'           => __( 'All Items', 'text_domain' ),
				'view_item'           => __( 'View Item', 'text_domain' ),
				'add_new_item'        => __( 'Add New Item', 'text_domain' ),
				'add_new'             => __( 'Add New', 'text_domain' ),
				'edit_item'           => __( 'Edit Item', 'text_domain' ),
				'update_item'         => __( 'Update Item', 'text_domain' ),
				'search_items'        => __( 'Search Item', 'text_domain' ),
				'not_found'           => __( 'Not found', 'text_domain' ),
				'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
			);
			$args = array(
				'label'               => __( '', 'text_domain' ),
				'description'         => __( 'Post Type Partners', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'thumbnail', ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 5,
				'menu_icon' 			=>  'dashicons-businessman',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			);
			register_post_type( 'partners', $args );
		}
	}
	
	
	add_action( 'init', 'register_testimonial_post_type' ); 

	if ( ! function_exists( 'register_testimonial_post_type' ) ) {
		function register_testimonial_post_type () {
			$labels = array(
				'name'                => _x( 'testimonials', 'Post Type General Name', 'text_domain' ),
				'singular_name'       => _x( 'testimonial', 'Post Type Singular Name', 'text_domain' ),
				'menu_name'           => __( 'Testimonials', 'text_domain' ),
				'parent_item_colon'   => __( 'Parent Post:', 'text_domain' ),
				'all_items'           => __( 'All Posts', 'text_domain' ),
				'view_item'           => __( 'View Post', 'text_domain' ),
				'add_new_item'        => __( 'Add New Post', 'text_domain' ),
				'add_new'             => __( 'Add New', 'text_domain' ),
				'edit_item'           => __( 'Edit Post', 'text_domain' ),
				'update_item'         => __( 'Update Post', 'text_domain' ),
				'search_items'        => __( 'Search Post', 'text_domain' ),
				'not_found'           => __( 'Not found', 'text_domain' ),
				'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
			);
			$args = array(
				'label'               => __( '', 'text_domain' ),
				'description'         => __( 'Post Type Testimonials', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'thumbnail' ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 5,
				'menu_icon' 			=>  'dashicons-format-aside',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			);
			register_post_type( 'testimonials', $args );
		}
	}
	
	
	add_action( 'init', 'register_portfolio_post_type' ); 

	if ( ! function_exists( 'register_portfolio_post_type' ) ) {
		function register_portfolio_post_type () {
			$labels = array(
				'name'                => _x( 'portfolio', 'Post Type General Name', 'text_domain' ),
				'singular_name'       => _x( 'portfolio', 'Post Type Singular Name', 'text_domain' ),
				'menu_name'           => __( 'Portfolio', 'text_domain' ),
				'parent_item_colon'   => __( 'Parent Post:', 'text_domain' ),
				'all_items'           => __( 'All Posts', 'text_domain' ),
				'view_item'           => __( 'View Post', 'text_domain' ),
				'add_new_item'        => __( 'Add New Post', 'text_domain' ),
				'add_new'             => __( 'Add New', 'text_domain' ),
				'edit_item'           => __( 'Edit Post', 'text_domain' ),
				'update_item'         => __( 'Update Post', 'text_domain' ),
				'search_items'        => __( 'Search Post', 'text_domain' ),
				'not_found'           => __( 'Not found', 'text_domain' ),
				'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
			);
			$args = array(
				'label'               => __( '', 'text_domain' ),
				'description'         => __( 'Post Type Portfolio', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'editor', 'thumbnail','excerpt' ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 5,
				'menu_icon' 			=>  'dashicons-welcome-widgets-menus',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			);
			register_post_type( 'Portfolio', $args );
		}
	}
	
	
	if ( ! function_exists( 'create_portfolio_taxonomies' ) ) {
		
		add_action( 'init', 'create_portfolio_taxonomies', 0 );

		function create_portfolio_taxonomies() {
		  // Add new taxonomy, make it hierarchical (like categories)
		  $labels = array(
			'name' => _x( 'Categories', 'taxonomy general name' ),
			'singular_name' => _x( 'portfolio-cat', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Genres' ),
			'all_items' => __( 'All Categories' ),
			'parent_item' => __( 'Parent category' ),
			'parent_item_colon' => __( 'Parent category:' ),
			'edit_item' => __( 'Edit category' ), 
			'update_item' => __( 'Update category' ),
			'add_new_item' => __( 'Add New category' ),
			'new_item_name' => __( 'New category Name' ),
			'menu_name' => __( 'Categories' ),
		  ); 	

		  register_taxonomy('portfolio-categories','portfolio', array(
			'hierarchical' => true,
			'labels' => $labels,
		  ));
		}
	}

	if ( ! function_exists( 'rst_class' ) ) {
		add_action( 'init', 'rst_class', 0 );
		function rst_class($args) {
			$class = '';
			if( !is_array($args) ) return '';
			foreach($args as $item) $class .= $item .' ';
			return trim($class);
		}
	}
	
	function rst_js_composer_init() {
			
		
		// Visual Composer HOOKS
		function rd_check_vc_status() {
			include_once(ABSPATH.'wp-admin/includes/plugin.php');
			if(is_plugin_active('js_composer/js_composer.php')) {
			 return true;
			}
			else{
				return false;
			}
		}
		
		// V Composer
		if( rd_check_vc_status() ) {
			include( MULTIUP_PLUGIN_DIR. '/vc_functions.php');
		}
		
		/*	Add custom control for Visual Composer	*/


		if ( class_exists('WPBakeryVisualComposerAbstract') ) {
			
			// Add Type
			require MULTIUP_PLUGIN_DIR . '/vc_extend/type/ublog_type.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/type/icons.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/type/postlist.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/type/animate.php';
			
			// Add Shortcode
			require MULTIUP_PLUGIN_DIR . '/vc_extend/icon-box.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/box-navigation.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/contact-box-info.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/custom-map.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/gallery.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/group-blog.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/count-to.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/heading.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/pricing.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/progressbar.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/scroll-down.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/slider-images.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/team.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/video.php';
			
			require MULTIUP_PLUGIN_DIR . '/vc_extend/page-blog.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/accordions.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/alert.php';
			require MULTIUP_PLUGIN_DIR . '/vc_extend/button.php';
			
			if( post_type_exists( 'testimonials' ) ) {
				require MULTIUP_PLUGIN_DIR . '/vc_extend/testimonial.php';
				require MULTIUP_PLUGIN_DIR . '/vc_extend/slider-testimonial.php';
			}
			if( post_type_exists( 'partners' ) ) {
				require MULTIUP_PLUGIN_DIR . '/vc_extend/slider-partners.php';
			}
			if( post_type_exists( 'portfolio' ) ) {
				require MULTIUP_PLUGIN_DIR . '/vc_extend/group-portfolio.php';
			}

		}
		
	}
	add_action('init','rst_js_composer_init', 999);
	 