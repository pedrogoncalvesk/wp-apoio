<?php
$attributes = array(
	"type" => "dropdown",
      "heading" => __('Container', LANGUAGE_ZONE),
      "param_name" => "select_width",
      "value" => array(
						__("Container", LANGUAGE_ZONE) => '2',
                        __("Full Width", LANGUAGE_ZONE) => '1',
                        __("Small Container", LANGUAGE_ZONE) => '3',
                      ),
      "description" => __("Select width of container", LANGUAGE_ZONE),
);
vc_add_param('vc_row', $attributes);